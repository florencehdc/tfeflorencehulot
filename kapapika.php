<!DOCTYPE html>
<html lang="fr">

  <head>

  	<meta charset="utf-8">

  	<title>Ekool</title>

    <!-- Jquery -->
    <script src="/js/librairies/jquery-3.2.1.min.js"></script>
    <script src ="/js/librairies/js.cookie.js"></script>

    <!-- GSAP -->
      <!-- TweenMax -->
      <script src="/js/librairies/GSAP/TweenMax.min.js"></script>
      <!-- GSAP plugins -->
        <!-- ScrollMagic -->
        <script src="/js/librairies/GSAP/plugins/ScrollMagic/ScrollMagic.min.js"></script>
          <!-- ScrollMagic plugins -->
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/animation.gsap.min.js"></script>
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/animation.velocity.min.js"></script>
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/debug.addIndicators.min.js"></script>
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/jquery.ScrollMagic.min.js"></script>
        <!-- ScrollToPlugin -->
        <script src="/js/librairies/GSAP/plugins/ScrollToPlugin.min.js"></script>

    <!-- CSS Style-->
    <link href="/css/screen.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,|Quicksand:300,400,500,700,|Luckiest+Guy:400" rel="stylesheet">

  </head>

    <!-- JS -->
      <!-- Animations -->
      <script src="/js/planets/Kapapika.js"></script>
      <!-- JS all scripts -->
      <script src="/js/master.js"></script>
      <!-- Serious Game -->

    <body>

      <!-- Audio -->

      <audio id="audioPlayer_kapapika">
        <source src="/audio/home.ogg">
        <source src="/audio/home.mp3">
      </audio>

      <button type="button" id="mute2" name="mute2"></button>

      <!-- Alerte 30 min d'utilisation -->

      <div class="alert-tps alert--hide">
        <div class="alert-tps__container">
          <div class="alert-tps__message">
            <p> Attention, tu es devant ton écran depuis 30 minutes.
                <br>
                <br> Waarschuwing, u bent in de voorkant van je scherm gedurende 30 minuten.
            </p>
          </div>
          <div class="alert-tps__boutons">
            <p><button type="button" name="button" class="closeAlert"> OK </button></p>
            <!--<p><a href="http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm" target="_blank"><button type="button" name="button" class="closeMessage"> En savoir plus </button></a></p>-->
          </div>
        </div>
        <div class="alert-tps__cache"></div>
      </div>

      <div class="page Kapapika">

        <div class="scrollDown--kapapika" id="scrollDown">
          <p>scroll<br><br><img src="/img/scroll_kapapika.svg" alt="scroll down"></p>
        </div>

        <div class="K-immersif">
          <?php include 'img/Kapapika/immersif/bloc1.php';?>
          <div class="K-immersif__navigation">
            <ul>
              <li>
                <a href="#.html">
                  <img src="img/Kapapika/immersif/fleche-gauche.svg" alt="plastique">
                </a>
              </li>
              <li>
                <a href="#.html">
                  <img src="img/Kapapika/immersif/fleche-droite.svg" alt="plastique">
                </a>
              </li>
            </ul>
          </div>
          <div class="K-immersif__visuel">

          </div>
        </div>

        <div class="backindex" id="backindex">
          <a href="index.php">
            <img src="/img/retour_menu_kapapika.svg" alt="back homepage">
          </a>
        </div>

        <div class="K-module-interstitiel--1"></div>

        <div class="nouveau-module K-presentation--part1" id="trigger--K-presentation--part1">
          <div class="K-presentation--part1__bg">
            <?php include 'img/Kapapika/modules/1_presentation/bloc2.php';?>
          </div>
          <div class="module-image--1">
            <div class="module-image__texte">
              <div class="module-image__title text-color--jaune" id="trigger--backindex">
                <p data-t-id="K-presentation__title--part1"> De quelle couleur est la poubelle des papiers et cartons ? </p>
              </div>
            </div>
            <div class="module-image__image" id="K-presentation--part1__poubelle">
              <ul>
                <li>
                  <img src="/img/Kapapika/modules/1_presentation/poubelle_jaune.svg"/>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="nouveau-module K-presentation--part2" id="trigger--K-presentation--part2">
          <div class="K-presentation--part2__bg">
            <?php include 'img/Kapapika/modules/1_presentation/bloc3.php';?>
          </div>
          <div class="module-text">
            <div class="module-text__texte bg-image--picture bg-position--centerbottom padding--bottom9">
                <div class="module-text__title text-color--jaune">
                  <p data-t-id="K-presentation__title--part2"> Que met-on dans cette poubelle ? </p>
                </div>
                <div class="module-text__intro">
                  <p data-t-id="K-presentation__intro--part2"> Il est important de bien suivre les consignes données lors du tri des déchets ! </p>
                </div>
                <div class="module-text__body padding--bottom10">
                  <p data-t-id="K-presentation__body--part2"> En effet, si l'on ne respecte pas ces consignes, cela peut créer des problèmes et perturber fortement le travail réalisé dans les usines de tri. </p>
                </div>
            </div>
            <div class="element-animation--1" id="K-presentation--part2__emoji">
              <img src="/img/Kapapika/modules/1_presentation/pouce_bas.svg" alt="pas bien">
            </div>
          </div>
        </div>

        <div class="nouveau-module K-presentation--part3" id="trigger--K-presentation--part3">
          <div class="K-presentation--part3__bg">
            <?php include 'img/Kapapika/modules/1_presentation/bloc4.php';?>
          </div>
          <div class="module-image--3">
            <div class="module-image__texte">
              <div class="module-image__intro">
                <p data-t-id="K-presentation__intro--part3"> CE QUE JE PEUX METTRE : </p>
              </div>
            </div>
            <div class="module-image__image">
              <ul>
                <li id="K-presentation--part3__li1">
                  <img src="/img/Kapapika/modules/1_presentation/bien_1.svg"/>
                  <p data-t-id="K-presentation__li1--part3"> Sacs en papier et boîtes en carton </p>
                </li>
                <li id="K-presentation--part3__li2">
                  <img src="/img/Kapapika/modules/1_presentation/bien_2.svg"/>
                  <p data-t-id="K-presentation__li2--part3"> Journaux, revues, dépliants </p>
                </li>
                <li id="K-presentation--part3__li3">
                  <img src="/img/Kapapika/modules/1_presentation/bien_3.svg"/>
                  <p data-t-id="K-presentation__li3--part3"> Papiers, papiers à lettre et pour imprimante </p>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="nouveau-module K-presentation--part4" id="trigger--K-presentation--part4">
          <div class="K-presentation--part4__bg">
            <?php include 'img/Kapapika/modules/1_presentation/bloc5.php';?>
          </div>
          <div class="module-image--3">
            <div class="module-image__texte bg-image--picture padding--top8 bg-position--centertop">
              <div class="module-image__intro text-color--blanc">
                <p data-t-id="K-presentation__intro--part4"> CE QUE JE NE PEUX PAS METTRE : </p>
              </div>
            </div>
            <div class="module-image__image">
              <ul>
                <li id="K-presentation--part4__li1">
                  <img src="/img/Kapapika/modules/1_presentation/mauvais_1.svg"/>
                  <p data-t-id="K-presentation__li1--part4"> Pas de cartons avec une couche d'alluminium </p>
                </li>
                <li id="K-presentation--part4__li2">
                  <img src="/img/Kapapika/modules/1_presentation/mauvais_2.svg"/>
                  <p data-t-id="K-presentation__li2--part4"> Pas de papiers ni de cartons souillés ou gras </p>
                </li>
                <li id="K-presentation--part4__li3">
                  <img src="/img/Kapapika/modules/1_presentation/mauvais_3.svg"/>
                  <p data-t-id="K-presentation__li3--part4"> Pas de papiers cellophane </p>
                </li>
              </ul>
            </div>
          </div>
        </div>

    <div class="module-interstitiel K-module-interstitiel--2bis">
      <div class="K-module-interstitiel--2bis__bg">
        <?php include 'img/Kapapika/modules/1_presentation/interstitiel.php';?>
      </div>
    </div>

        <div class="nouveau-module K-presentation--part5" id="trigger--K-presentation--part5">
          <div class="K-presentation--part5__bg">
            <?php include 'img/Kapapika/modules/1_presentation/bloc6.php';?>
          </div>
          <div class="module-text bg-image--picture bg-position--centerbottom padding--bottom2">
            <div class="module-text__texte">
                <div class="module-text__title text-color--jaune">
                  <p data-t-id="K-presentation__title--part5"> Pourquoi recycle-t-on le papier et le carton ? </p>
                </div>
                <div class="module-text__intro">
                  <p data-t-id="K-presentation__intro--part5"> Le recyclage permet de réutiliser la matière. De cette manière, on diminue la montagne de déchets et on préserve un meilleur équilibre dans la nature. </p>
                </div>
                <div class="module-text__body padding--bottom10">
                  <p data-t-id="K-presentation__body--part5"> En réutilisant au maximum une matière au lieu de tout simplement la jeter et devoir la recréer de toute pièce, on gaspille moins, on dépense moins d'énergie et on pollue moins. </p>
                </div>
            </div>
            <div class="element-animation--2 padding--bottom2" id="K-presentation--part5__emoji">
              <img src="/img/Kapapika/modules/1_presentation/pouce_haut.svg" alt="bien">
            </div>
          </div>
        </div>

        <div class="nouveau-module K-presentation--part6" id="trigger--K-presentation--part6">
          <div class="K-presentation--part6__bg">
            <?php include 'img/Kapapika/modules/1_presentation/bloc7.php';?>
          </div>
          <div class="module-image--3 bg-image--picture bg-position--leftcenter">
            <div class="module-image__texte text-color--jaune">
              <div class="module-image__intro">
                <p data-t-id="K-presentation__intro--part6"> Savais-tu que... ? </p>
              </div>
            </div>
            <div class="module-image__image">
              <ul>
                <li id="K-presentation--part6__li1">
                  <img src="/img/Kapapika/modules/1_presentation/journaux.svg"/>
                  <p data-t-id="K-presentation__li1--part6"> Si tout nos journaux étaient recyclés, ... </p>
                </li>
                <li id="K-presentation--part6__li2">
                  <img src="/img/Kapapika/modules/1_presentation/foret.svg"/>
                  <p data-t-id="K-presentation__li2--part6"> ... nous pourrions économiser environ 250.000.000 d’arbres dans le monde chaque <br>année ! </p>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="module-interstitiel K-module-interstitiel--2">
        </div>

        <div class="nouveau-module K-transformation" id="trigger--K-transformation">
          <div class="module-gallerie">
            <div class="module-gallerie__texte">
              <div class="module-gallerie__title text-color--jaune">
                <p data-t-id="K-transformation__title"> Comment transforme-t-on la matière ? </p>
              </div>
              <div class="module-gallerie__intro">
                <p data-t-id="K-transformation__intro"> Il existe différentes techniques, mais découvrons ensemble les différentes étapes de l'une d'entre elles ! </p>
              </div>
            </div>
            <div class="module-gallerie__image">
              <ul class="list-item">
                <li class="step1 item">
                  <div class="stepWrapper">
                    <div class="step">
                      <div class="stepFace front">
                        <div class="step1__intro text-color--blanc">
                          <p data-t-id="K-step1__intro"> Le papier et le carton sont d’abord triés. Ils seront utilisés pour différentes choses en fonction de leur qualité. </p>
                        </div>
                      </div>
                      <div class="stepface back">
                        <div class="step1__body">
                          <ul>
                            <li>
                              <p data-t-id="K-step1__li1"> Mauvaise qualité </p>
                              <img src="/img/Kapapika/modules/2_transformation/1_etoile.svg" alt="boîtes d'emballage">
                            </li>
                            <li>
                              <p data-t-id="K-step1__li2"> Bonne qualité </p>
                              <img src="/img/Kapapika/modules/2_transformation/5_etoiles.svg" alt="magazines">
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li class="step2 item">
                  <div class="stepWrapper">
                    <div class="step">
                      <div class="stepFace front">
                        <div class="step2__intro text-color--blanc">
                          <p data-t-id="K-step2__intro"> Le papier est ensuite mélangé avec de l’eau, pour obtenir une bouillie brunâtre appelée «pulpe». </p>
                        </div>
                      </div>
                      <div class="stepface back">
                        <div class="step2__body">
                          <ul>
                            <li>
                              <img src="img/Kapapika/modules/2_transformation/peinture-vert.svg" alt="step2">
                              <p data-t-id="K-step2__li1"> La peinture, les agrafes, le vernis, les restes de colle, le plastique et les cordes sont éliminés de la pulpe. </p>
                            </li>
                            <li>
                              <img src="img/Kapapika/modules/2_transformation/encre.svg" alt="step2">
                              <p data-t-id="K-step2__li2"> Pour refabriquer certains papiers, il faut aussi retirer l'encre qui reste dans la pulpe et la blanchir. </p>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li class="step3 item">
                  <div class="stepWrapper">
                    <div class="step">
                      <div class="stepFace front">
                        <div class="step3__intro text-color--blanc">
                          <p data-t-id="K-step3__intro"> Pour finir, la pulpe est étalée puis pressée et séchée sur un tapis. </p>
                        </div>
                      </div>
                      <div class="stepface back">
                        <div class="step3__body">
                          <p data-t-id="K-step3__body"> Cette dernière étape va former une sorte de feuille de papier géante que l'on va enrouler pour former plein de nouveaux rouleaux de papier ! </p>
                          <img src="img/Kapapika/modules/2_transformation/rouleau.svg" alt="step3">
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="module-interstitiel K-module-interstitiel--3">
          <div class="K-module-interstitiel--3__bg">
            <?php include 'img/Kapapika/modules/2_transformation/interstitiel1.php';?>
          </div>
        </div>

        <div class="nouveau-module K-resultat--part1" id="trigger--K-resultat--part1">
          <div class="K-resultat--part1__bg">
            <?php include 'img/Kapapika/modules/3_resultat/bloc9_resu1.php';?>
          </div>
          <div class="module-text-image--3">
            <div class="module-text-image__texte">
                <div class="module-text-image__title text-color--jaune">
                  <p data-t-id="K-resultat__title--part1"> Quel est le résultat de cette transformation ? </p>
                </div>
                <div class="module-text-image__intro">
                  <p data-t-id="K-resultat__intro--part1"> Le papier et le carton usagés deviennent simplement du papier et du carton neufs. </p>
                </div>
                <div class="module-text-image__body">
                  <p data-t-id="K-resultat__body--part1"> Nous sommes tous les jours en contact avec du papier et du carton recyclés. </p>
                </div>
            </div>
            <div class="module-text-image__image">
              <ul>
                <li id="K-resultat--part1__li1">
                  <img src="/img/Kapapika/modules/3_resultat/jour_mag.svg"/>
                  <p data-t-id="K-resultat__li1--part1"> Journaux et magazines </p>
                </li>
                <li id="K-resultat--part1__li2">
                  <img src="/img/Kapapika/modules/3_resultat/cahier.svg"/>
                  <p data-t-id="K-resultat__li2--part1"> Cahiers et papier à dessin </p>
                </li>
                <li id="K-resultat--part1__li3">
                  <img src="/img/Kapapika/modules/3_resultat/boite.svg"/>
                  <p data-t-id="K-resultat__li3--part1"> Boîtes en carton et cartons d'emballage </p>
                </li>
                <li id="K-resultat--part1__li4">
                  <img src="/img/Kapapika/modules/3_resultat/papier_toilette.svg"/>
                  <p data-t-id="K-resultat__li4--part1"> Essuie tout, papier toilette et mouchoirs </p>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="nouveau-module K-resultat--part2" id="trigger--K-resultat--part2 bg-position--center">
          <div class="K-resultat--part2__bg">
            <?php include 'img/Kapapika/modules/3_resultat/bloc10_resu2.php';?>
          </div>
          <div class="module-image--3">
            <div class="module-image__texte">
                <div class="module-image__intro">
                  <p data-t-id="K-resultat__intro--part2"> Mais la vie d’une boîte en carton n’est pas infinie. En effet, la qualité du matériau diminue après chaque traitement. En moyenne, les fibres peuvent être réutilisées cinq à sept fois. </p>
                </div>
            </div>
            <div class="module-image__image">
              <ul>
                <li id="K-resultat--part2__li1"></li>
              </ul>
            </div>
          </div>
        </div>

        <div class="module-interstitiel K-module-interstitiel--4">
          <div class="K-module-interstitiel--4__bg">
            <?php include 'img/Kapapika/modules/3_resultat/interstitiel2_Ap_cycle.php';?>
          </div>
        </div>

        <div class="nouveau-module K-tuto-recup" id="trigger--K-tuto-recup">
          <div class="module-text-video--3">
            <div class="module-text-video__texte">
                <div class="module-text-video__title text-color--jaune" id="K-tuto-recup">
                  <p data-t-id="K-tuto-recup__title"> Tuto récup'! </p>
                </div>
                <div class="module-text-video__intro">
                  <p data-t-id="K-tuto-recup__intro"> Tu peux toi aussi recycler le papier et le carton que tu n'utilises plus à la maison en le transformant en de nouvelles choses ! </p>
                </div>
                <div class="module-text-video__body">
                  <p data-t-id="K-tuto-recup__body"> Ici, tu peux découvrir comment réutiliser des chutes de carton et des rouleaux de papier toilette pour créer une boîte de rangement facilement ! </p>
                </div>
            </div>
            <div class="module-text-video__video">
              <object type="" width="974" height="532" data="https://player.vimeo.com/video/222670067">
                <param name="movie" value="https://player.vimeo.com/video/222670067"/>
              </object>
            </div>
          </div>
        </div>

        <div class="module-interstitiel K-module-interstitiel--5">
          <div class="K-module-interstitiel--5__bg">
            <?php include 'img/Kapapika/modules/4_tuto-recup/interstitiel3_Ap_video.php';?>
          </div>
        </div>

        <div class="nouveau-module K-serious-games" id="trigger--K-serious-games">
          <div class="K-serious-games__title text-color--jaune">
            <p data-t-id="K-serious-games__title"> Entraîne-toi et deviens un as du recyclage ! </p>
          </div>
          <div class="K-serious-games__games">
            <ul>
              <li id="K-serious-games__li1">
                <img src="/img/Kapapika/modules/5_jeux/logoJeu.svg" alt="#1">
                <p data-t-id="K-serious-games__li1"> Aide Pika à trier <br>les déchets </p>
                <!--<a href="#">-->
                  <p><button class="serious-games1" data-t-id="K-serious-games__li1--button" type="button" name="button"> START </button></p>
                <!--</a>-->
              </li>
              <li id="K-serious-games__li2">
                <img src="/img/Kapapika/modules/5_jeux/logoJeu2.svg" alt="#2">
                <p data-t-id="K-serious-games__li2"> Sauras-tu répondre correctement ? </p>
                <!--<a href="#">-->
                  <p><button class="serious-games2" data-t-id="K-serious-games__li2--button" type="button" name="button"> START </button></p>
                <!--</a>-->
              </li>
              <li id="K-serious-games__li3">
                <img src="/img/Kapapika/modules/5_jeux/logoJeu3.svg" alt="#3">
                <p data-t-id="K-serious-games__li3"> Trouves l'intru <br>rapidement ! </p>
                <!--<a href="#">-->
                  <p><button class="serious-games3" data-t-id="K-serious-games__li3--button" type="button" name="button"> START </button></p>
                <!--</a>-->
              </li>
            </ul>
          </div>

          <div class="K-serious-games__message-erreur message--hide">
            <p data-t-id="K-serious-games__message-erreur"> Désolé, ce jeu n'est pas encore disponible ! </p>
            <p><button data-t-id="K-serious-games__message-erreur--button" type="button" name="button" class="closeMessage"> OK </button></p>
          </div>

          <div class="K-serious-games__fenetreJeu fenetre--hide">
            <p><a href="#K-serious-games" class="closeJeu" title="Hide This Message">&times;</a></p>
            <canvas id="jeu" width="800" height="500"></canvas>
          </div>

        </div>

        <div class="module-interstitiel K-module-interstitiel--6">
          <div class="K-module-interstitiel--6__bg">
            <?php include 'img/Kapapika/modules/5_jeux/interstitiel4_Ap_jeux.php';?>
          </div>
        </div>

        <div class="nouveau-module K-telechargements" id="trigger--K-telechargements">
          <div class="K-telechargements__title text-color--jaune">
            <p data-t-id="K-telechargements__title"> Activités </p>
          </div>
          <div class="K-telechargements__intro">
            <p data-t-id="K-telechargements__intro"> Découvre des activités à réaliser chez toi en famille ou à l'école avec ta classe, et apprends aux autres ce que tu as appris ici. </p>
          </div>
          <div class="K-telechargements__liens">
            <ul>
              <li id="K-telechargements__li1">
                <a href="img/Kapapika/modules/6_downloads/fichiers/dog.jpg" download="activité1">
                  <img src="/img/Kapapika/modules/6_downloads/stickers.svg" alt="#1">
                </a>
                <p data-t-id="K-telechargements__li1"> Décore tes poubelles </p>
              </li>
              <li id="K-telechargements__li2">
                <a href="img/Kapapika/modules/6_downloads/fichiers/dog.jpg" download="activité2">
                  <img src="/img/Kapapika/modules/6_downloads/stickers2.svg" alt="#2">
                </a>
                <p data-t-id="K-telechargements__li2"> Le grand jeu du tri </p>
              </li>
              <li id="K-telechargements__li3">
                <a href="img/Kapapika/modules/6_downloads/fichiers/dog.jpg" download="activité3">
                  <img src="/img/Kapapika/modules/6_downloads/stickers3.svg" alt="#2">
                </a>
                <p data-t-id="K-telechargements__li3"> Pika a besoin de toi ! </p>
              </li>
            </ul>
          </div>
        </div>

        <div class="module-interstitiel K-module-interstitiel--7">
          <div class="K-module-interstitiel--7__bg">
            <?php include 'img/Kapapika/modules/6_downloads/interstitiel4_Ap_download.php';?>
          </div>
        </div>

        <div class="nouveau-module K-evaluation" id="trigger--K-evaluation">
          <div class="K-evaluation__title text-color--jaune">
            <p data-t-id="K-evaluation__title"> Que penses-tu de ce site ? </p>
          </div>
          <div class="K-evaluation__intro">
            <p data-t-id="K-evaluation__intro"> A toi de nous apprendre quelque chose maintenant <br>et dis-nous comment tu trouves Ekool ! </p>
          </div>
          <div class="K-evaluation__emojis">
            <ul class="emojis__list">
              <li class="emojis__item K-evaluation__emojis--1">
                <button type="radio" value="1/5" name="1/5" class="emojis curseur--hide " id="K-evaluation__li1" data-emoji-value="1" alt="1/5">
                  <img class="curseur" src="/img/Kapapika/modules/7_evaluation/curseur.png" alt="curseur">
                </button>
              </li>
              <li class="emojis__item K-evaluation__emojis--2">
                <button type="radio" value="2/5" name="2/5" class="emojis curseur--hide" id="K-evaluation__li2" data-emoji-value="2" alt="2/5">
                  <img class="curseur" src="/img/Kapapika/modules/7_evaluation/curseur.png" alt="curseur">
                </button>
              </li>
              <li class="emojis__item K-evaluation__emojis--3">
                <button type="radio"  value="3/5" name="3" class="emojis curseur--hide" id="K-evaluation__li3" data-emoji-value="3/5" alt="3/5">
                  <img class="curseur" src="/img/Kapapika/modules/7_evaluation/curseur.png" alt="curseur">
                </button>
              </li>
              <li class="emojis__item K-evaluation__emojis--4">
                <button type="radio" value="4/5" name="4/5" class="emojis curseur--hide" id="K-evaluation__li4" data-emoji-value="4" alt="4/5">
                  <img class="curseur" src="/img/Kapapika/modules/7_evaluation/curseur.png" alt="curseur">
                </button>
              </li>
              <li class="emojis__item K-evaluation__emojis--5">
                <button type="radio" value="5/5" name="5/5" class="emojis curseur--hide" id="K-evaluation__li5" data-emoji-value="5" alt="5/5">
                  <img class="curseur" src="/img/Kapapika/modules/7_evaluation/curseur.png" alt="curseur">
                </button>
              </li>
            </ul>
          </div>
          <div class="K-evaluation__validation">
            <button type="submit" name="button" data-t-id="K-evaluation__validation"> Je valide ! </button>
          </div>
        </div>

        <div class="footer" id="trigger--footer">
          <div class="footer__bg">
            <?php include 'img/Kapapika/modules/bloc_fin.php';?>
          </div>
          <div class="footer__navigation">
            <ul>
              <li class="footer__navigation--exterieur">
                <a href="#.html">
                  <img src="/img/Kapapika/footer/immersif-footer/footer_G.svg" alt="plastique">
                </a>
              </li>
              <li class="footer__navigation--centre">
                <div class="navigation__a-propos">
                    <img src="/img/Kapapika/footer/about/a-propos.svg" alt="À propos">
                  <div class="navigation__a-propos--img">
                  </div>
                  <div class="navigation__a-propos--button">
                    <p><a href="about.php"><button data-t-id="naviguation__a-propos--button" type="button" name="button"> À propos </button></a></p>
                  </div>
                </div>
                <div class="navigation__rejoins-nous">
                  <ul>
                    <li><p data-t-id="naviguation__rejoins-nous"> Rejoins-nous sur </p></li>
                    <li>
                      <a href="https://www.facebook.com/" class="facebook">Facebook</a>
                    </li>
                    <li>
                      <a href="https://www.instagram.com/" class="instagram">Instagram</a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="footer__navigation--exterieur">
                <a href="#.html">
                  <img src="img/Kapapika/footer/immersif-footer/footer_D.svg" alt="plastique">
                </a>
              </li>
            </ul>
          </div>
          <div class="footer__copyright--docs text-color--blanc">
            <p data-t-id="footer__copyright">© 2017 Ekool - Tous droits réservés</p>
          </div>
        </div>

      </div>

      <script src="/jeu/js/main.js"></script>

    </body>

</html>
