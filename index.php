<!DOCTYPE html>
<html lang="fr">

  <head>

  	<meta charset="utf-8">

  	<title>Ekool</title>

    <!-- Jquery -->
    <script src="/js/librairies/jquery-3.2.1.min.js"></script>
    <script src ="/js/librairies/js.cookie.js"></script>

    <!-- GSAP -->
      <!-- TweenMax -->
      <script src="/js/librairies/GSAP/TweenMax.min.js"></script>
      <!-- GSAP plugins -->
        <!-- ScrollMagic -->
        <script src="/js/librairies/GSAP/plugins/ScrollMagic/ScrollMagic.min.js"></script>
          <!-- ScrollMagic plugins -->
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/animation.gsap.min.js"></script>
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/animation.velocity.min.js"></script>
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/debug.addIndicators.min.js"></script>
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/jquery.ScrollMagic.min.js"></script>
        <!-- ScrollToPlugin -->
        <script src="/js/librairies/GSAP/plugins/ScrollToPlugin.min.js"></script>

    <!-- CSS Style-->
    <link href="/css/screen.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,|Quicksand:300,400,500,700,|Luckiest+Guy:400" rel="stylesheet">

  </head>

    <!-- JS -->
      <!-- Popup -->
      <script src="/js/homepage/popup.js"></script>
      <!-- Animations -->
      <script src="/js/homepage/index.js"></script>
      <!-- JS all scripts -->
      <script src="/js/master.js"></script>

    <body>

      <!-- Alerte NO-SCRIPT -->

      <noscript id="noscript">
          <img src="/img/homepage/background/noscript.svg" alt="noscript">
          <p>
            Votre navigateur n'est malheureusement pas dans la capacité de lire les scripts liés à ce site internet, ou n'en reconnaît pas les langages. Pour une expérience optimale, veuillez utliser un navigateur adapté.
            <br>
            <br>Uw browser kan de voor deze site gebruikte scripts en programmeertaal niet erkennen. Voor een optimale navigatie, gelieve een andere browser te gebruiken.
          </p>
      </noscript>

      <!-- Test LOADER -->

      <script>
      /*
      var loader = document.createElement('');
      loader.src = "img/";
      loader.style.cssText = '';
      */
            function bodyLoader() {
                /*document.body.appendChild(loader);
                loader.play();*/
                console.log("window onload");
            }

            window.addEventListener("load", function() {
                /*document.body.removeChild(loader);*/
                console.log("window loaded")
            });
      </script>

      <!-- Alerte utilisation COOKIES -->

      <div class="alert-cookies alert--hide">
        <div class="alert-cookies__container">
          <div class="alert-cookies__message">
            <p> Ekool utilise des cookies pour vous offrir le meilleur service. En poursuivant votre navigation, vous acceptez l'utilisation de cookies sur ce site.
                <br>
                <br> Ekool gebruikt cookies om u de beste service te bieden. Door verder te browsen accepteert u het gebruik van cookies op deze site.
            </p>
          </div>
          <div class="alert-cookies__boutons">
            <p><button type="button" name="button" class="closeAlert"> OK </button></p>
            <!--<p><a href="http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm" target="_blank"><button type="button" name="button" class="closeMessage"> En savoir plus </button></a></p>-->
          </div>
        </div>
        <div class="alert-cookies__cache"></div>
      </div>

      <!-- Audio -->

      <audio id="audioPlayer">
        <source src="/audio/home2.ogg">
        <source src="/audio/home2.mp3">
      </audio>

      <button type="button" id="mute" name="mute"></button>

      <!-- Alerte 30 min d'utilisation -->

      <div class="alert-tps alert--hide">
        <div class="alert-tps__container">
          <div class="alert-tps__message">
            <p> Attention, tu es devant ton écran depuis 30 minutes.
                <br>
                <br> Waarschuwing, u bent in de voorkant van je scherm gedurende 30 minuten.
            </p>
          </div>
          <div class="alert-tps__boutons">
            <p><button type="button" name="button" class="closeAlert"> OK </button></p>
            <!--<p><a href="http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm" target="_blank"><button type="button" name="button" class="closeMessage"> En savoir plus </button></a></p>-->
          </div>
        </div>
        <div class="alert-tps__cache"></div>
      </div>

      <div class="page index">

        <div class="popup change-state--on-click popup--hide" id="modal">
          <div class="popup__languages">
            <ul class="languages__list">
              <li class="languages__item">
                <button id="FR" data-button-value="fr" data-language-id="fr" class="languages--inactive"><p>FR</p></button>
              </li>
              <li class="languages__item">
                <button id="NL" data-button-value="nl" data-language-id="nl" class="languages--inactive"><p>NL</p></button>
              </li>
            </ul>
          </div>
          <div id="modal__texte" class="popup__texte">

            <div class="popup__texte">
              <div class="texte__title">
                <p data-t-id="popup__title"> Bienvenue sur </p>
              </div>
              <div class="texte__logo">
                <img src="/img/homepage/popup/LOGO/LOGO.svg" alt="Ekool">
              </div>
              <div class="texte__body">
                <p data-t-id="popup__body"> Apprends en t'amusant et deviens un as du recyclage ! <br> Tu rejoindras alors la brigade d'Ekool ! </p>
              </div>
              <div class="popup__close">
                <button type="button" class="close" name="button" data-t-id="popup__close"> Bonne découverte ! </button>
              </div>
            </div>
          </div>
        </div>

        <div class="scrollDown" id="scrollDown">
          <p>scroll<br><br><img src="/img/scroll_about_homepage.svg" alt="scroll down"></p>
        </div>

        <div class="index__background">
          <div class="background__clouds clouds--1" id="clouds1">
            <img src="../img/homepage/background/clouds_1.svg">
          </div>
          <div class="background__clouds clouds--2" id="clouds2">
            <img src="../img/homepage/background/clouds_2.svg">
          </div>
          <div class="background__clouds clouds--3" id="clouds3">
            <img src="../img/homepage/background/clouds_3.svg">
          </div>
          <div class="background__clouds clouds--4" id="clouds4">
            <img src="../img/homepage/background/clouds_4.svg">
          </div>
          <div class="background__clouds clouds--5" id="clouds5">
            <img src="../img/homepage/background/clouds_2.svg">
          </div>
          <div class="background__clouds clouds--6" id="clouds6">
            <img src="../img/homepage/background/clouds_1.svg">
          </div>
          <div class="background__clouds clouds--7" id="clouds7">
            <img src="../img/homepage/background/clouds_4.svg">
          </div>
          <div class="background__clouds clouds--8" id="clouds8">
            <img src="../img/homepage/background/clouds_2.svg">
          </div>
        </div>

        <div class="index__body">

          <div class="navigation">
            <ul>
              <li></li>
              <li>
                <div class="Ekool">
                  <img src="/img/homepage/LOGO/LOGO.svg" alt="Ekool">
                </div>
                <div class="background__stars">
                  <div class="stars--33">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--34">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--35">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--36">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                  <div class="stars--14">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--15">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--16">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--17">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--18">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                  <div class="stars--28">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--29">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--30">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                  <div class="stars--31">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                  <div class="stars--32">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                </div>
              </li>
              <li id="planet1">
                  <div class="navigation__illu">
                  <a href="kapapika.php">
                    <div class="js-rotate">
                      <?php include 'img/homepage/planets/Kapapika/planète_kapapika.php';?>
                    </div>
                  </a>
                </div>
                <div class="navigation__legende text-color--blanc">
                  <div class="navigation__legende--titre text-color--yellow">
                    <p id="triggerPlanet1"> Kapapika </p>
                  </div>
                  <div class="navigation__legende--intro">
                    <p data-t-id="intro--planet1"> Pika adore se déguiser. <br>Sauras-tu le retrouver ? </p>
                  </div>
                </div>
                <div class="background__stars" id="stars__planet1">
                  <div class="stars--1">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--2">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--3">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--4">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                  <div class="stars--5">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--6">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--7">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--8">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--9">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                </div>
              </li>
              <li id="planet2">
                <div class="navigation__illu">
                  <a href="#">
                    <div class="js-rotate">
                      <?php include 'img/homepage/planets/Plastika/planète_plastika.php';?>
                    </div>
                  </a>
                </div>
                <div class="navigation__legende text-color--blanc">
                  <div class="navigation__legende--titre text-color--blue">
                    <p> Plastika </p>
                  </div>
                  <div class="navigation__legende--intro">
                    <p data-t-id="intro--planet2"> Tika est une vraie arsouille. <br>Découvre son monde et tu repartiras avec plein d'astuces ! </p>
                  </div>
                </div>
                <div class="background__stars" id="stars__planet2">
                  <div class="stars--10">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--11">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--12">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--13">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                  <div class="stars--14">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--15">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--16">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--17">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--18">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                </div>
              </li>
              <li id="planet3">
                <div class="navigation__illu">
                  <a href="#">
                    <div class="js-rotate">
                      <?php include 'img/homepage/planets/Brolaka/planète_brolaka.php';?>
                    </div>
                  </a>
                </div>
                <div class="navigation__legende text-color--blanc">
                  <div class="navigation__legende--titre text-color--grey">
                    <p> Brolaka </p>
                  </div>
                  <div class="navigation__legende--intro">
                    <p data-t-id="intro--planet3"> Laka est parfois incompris et débordé. Aide-le à s'y retrouver ! </p>
                  </div>
                </div>
                <div class="background__stars" id="stars__planet3">
                  <div class="stars--19">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--20">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--21">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--22">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                  <div class="stars--23">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--24">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--25">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--26">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--27">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                </div>
              </li>
              <li id="planet4">
                <div class="navigation__illu">
                  <a href="#">
                    <div class="js-rotate">
                      <?php include 'img/homepage/planets/Verraka/planète_verraka.php';?>
                    </div>
                  </a>
                </div>
                <div class="navigation__legende text-color--blanc">
                  <div class="navigation__legende--titre text-color--bluegreen">
                    <p data-t-id="titre--planet4"> Verraka </p>
                  </div>
                  <div class="navigation__legende--intro">
                    <p data-t-id="intro--planet4"> Raka adore la musique. Découvre son univers musical ! </p>
                  </div>
                </div>
                <div class="background__stars" id="stars__planet4">
                  <div class="stars--1">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--2">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--3">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--4">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                  <div class="stars--5">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--6">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--7">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--8">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--9">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                </div>
              </li>
              <li id="planet5">
                <div class="navigation__illu">
                  <a href="#">
                    <div class="js-rotate">
                      <?php include 'img/homepage/planets/Organika/planète_organika.php';?>
                    </div>
                  </a>
                </div>
                <div class="navigation__legende text-color--blanc">
                  <div class="navigation__legende--titre text-color--green">
                    <p> Organika </p>
                  </div>
                  <div class="navigation__legende--intro">
                    <p data-t-id="intro--planet5"> Nika à la main verte. <br>Toi aussi peut-être ? </p>
                  </div>
                </div>
                <div class="background__stars" id="stars__planet5">
                  <div class="stars--10">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--11">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--12">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--13">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                  <div class="stars--14">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--15">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--16">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--17">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--18">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                </div>
              </li>
              <li id="planet6" id="stars__planet6">
                <div class="navigation__illu">
                  <a href="#">
                    <div class="js-rotate">
                      <?php include 'img/homepage/planets/Elektra/planète_elektra.php';?>
                    </div>
                  </a>
                </div>
                <div class="navigation__legende text-color--blanc">
                  <div class="navigation__legende--titre text-color--red">
                    <p> Elektra </p>
                  </div>
                  <div class="navigation__legende--intro">
                    <p data-t-id="intro--planet6"> Lektra est une vrai pile électrique ! Sauras-tu la suivre ? </p>
                  </div>
                </div>
                <div class="background__stars">
                  <div class="stars--19">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--20">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--21">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--22">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                  <div class="stars--23">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--24">
                    <img src="../img/homepage/background/stars_2.png">
                  </div>
                  <div class="stars--25">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--26">
                    <img src="../img/homepage/background/stars_1.png">
                  </div>
                  <div class="stars--27">
                    <img src="../img/homepage/background/stars_3.png">
                  </div>
                </div>
              </li>
              <li></li>
            </ul>
          </div>

          <div class="lien-about">
              <div class="lien-about__img">
                <?php include 'img/homepage/about/a-propos.php';?>
              </div>
              <div class="lien-about__button">
                <p><a href="about.php"><button data-t-id="lien-about__button" type="button" name="button"> À propos </button></a></p>
              </div>
          </div>

          <div class="footer--home">
            <div class="footer__copyright--home">
              <p data-t-id="footer__copyright"> © 2017 Ekool - Tous droits réservés </p>
            </div>
          </div>
          <div class="background__stars--footer">
            <div class="stars--10">
              <img src="../img/homepage/background/stars_1.png">
            </div>
            <div class="stars--11">
              <img src="../img/homepage/background/stars_2.png">
            </div>
            <div class="stars--12">
              <img src="../img/homepage/background/stars_2.png">
            </div>
            <div class="stars--13">
              <img src="../img/homepage/background/stars_3.png">
            </div>
            <div class="stars--14">
              <img src="../img/homepage/background/stars_1.png">
            </div>
            <div class="stars--15">
              <img src="../img/homepage/background/stars_2.png">
            </div>
            <div class="stars--16">
              <img src="../img/homepage/background/stars_1.png">
            </div>
            <div class="stars--17">
              <img src="../img/homepage/background/stars_1.png">
            </div>
            <div class="stars--18">
              <img src="../img/homepage/background/stars_3.png">
            </div>
          </div>
        </div>

      </div>

    </body>

</html>
