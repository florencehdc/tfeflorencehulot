/*################################################*/
/* = Test Cookies
/*################################################*/

var langueChoisie = 'fr'; //fr par defaut
if(Cookies.get('selected_locale') !== undefined)
{
  langueChoisie = Cookies.get('selected_locale');
}

/*################################################*/
/* = Traductions
/*################################################*/

var translation = {
  "fr": {

    /* Page1 */
    "miniJeu1__intro1": "Teste tes connaissances dans ce petit jeu en plaçant",
    "miniJeu1__intro2": "le déchet dans la bonne poubelle.",
    "miniJeu1__consignes1": "Le principe est simple. Avec les flèches de ton clavier,",
    "miniJeu1__consignes2": "mets le déchet dans la bonne poubelle.",
    "miniJeu1__start": "Appuie sur la barre d’espace pour commencer.",

    /* Page2 */
    "miniJeu1__score": "Score",
    "miniJeu1__best": "Meilleur score",

    /* Page3 */
    "miniJeu1__scoreNiveau1": "Oups ! As-tu été suffisamment attentif?",
    "miniJeu1__points": "points",
    "miniJeu1__scoreNiveau2": "Pas mal du tout ! Mais je suis sure que tu peux encore mieux faire !",
    "miniJeu1__scoreNiveau3": "Bravo ! Tu es un as du recyclage !",
    "miniJeu1__bestScore": "Ton meilleur score est",
    "miniJeu1__restart": "Appuie sur la barre d'espace pour recommencer.",

  },

  "nl":{

    /* Page1 */
    "miniJeu1__intro1": "Test je kennis in dit speltje door het",
    "miniJeu1__intro2": "afval in de juiste vuilnisbak te zetten.",
    "miniJeu1__consignes1": "Het beginsel is simpel. Met de pijlen van je toetsenbord,",
    "miniJeu1__consignes2": "zet de afval in juiste vuilnisbak.",
    "miniJeu1__start": "Druk op de spatiebalk om te starten.",

    /* Page2 */
    "miniJeu1__score": "Score",
    "miniJeu1__best": "Beste score",

    /* Page3 */
    "miniJeu1__scoreNiveau1": "Oeps! Ben je genoeg aandachtig geweest?",
    "miniJeu1__points": "punten",
    "miniJeu1__scoreNiveau2": "Goed gedaan! Maar ik ben zeker dans je nog beter kan!",
    "miniJeu1__scoreNiveau3": "Proficiat! Je bent een krak in recycling!",
    "miniJeu1__bestScore": "Je beste score is",
    "miniJeu1__restart": "Druk op de spatiebalk om te herbeginnen.",

  }
};

/*################################################*/
/* = Jeu
/*################################################*/

var canvas = document.getElementById("jeu");
var ctx = canvas.getContext('2d');

var gameState = "menu";

// création d'une variable poubelle qui sera un tableau dans lequel on ajoutera les boubelles à ajouter.
var poubelles = [];

var allowKey = false;

// création d'une variable localStorage afin de stocker le meilleur score effectué avec l'ordinateur
var stockage = localStorage;
var score = 0;

var bestScore = stockage.getItem("monScore");

var chrono = document.querySelector('p.chrono');

var objDessin = [];

//création des variables utiles à la création du compte à rebour
var remainingTime;
var lastTime = new Date().getTime();
var newTime, delta;


var logo = new Image();
logo.src = '/jeu/img/titreJeu2.png';

var FG = new Image();
FG.src = '/jeu/img/FG-01.png';
var FH = new Image();
FH.src = '/jeu/img/FG-02.png';
var FD = new Image();
FD.src = '/jeu/img/FG-03.png';

// création d'une fonction qui enlevera le temps delta au temps initial (remainingTime) afin d'aficher le temps qui passe en seconde
function checkTime(dt){
  remainingTime -= dt;
  // étant donné que le temps est en millisecondes, on le divise par 1000 afin de l'aficher en secondes et Math.ceil afin d'avoir un chiffre rond
  var secTime  = Math.ceil(remainingTime/1000);
  ctx.font = "100 80px Luckiest Guy";
  ctx.fillStyle = "#FFFFFF";
  ctx.textAlign = "center";
  ctx.fillText(secTime,canvas.width/2,500);

  ctx.font = "100 24px Luckiest Guy";
  ctx.fillStyle = "#5aa38b";
  ctx.textAlign = "center";
  ctx.fillText(translation[langueChoisie]['miniJeu1__score'],60,45);

  ctx.font = "100 60px Luckiest Guy";
  ctx.fillStyle = "#5aa38b";
  ctx.textAlign = "center";
  ctx.fillText(score,60,100);

  ctx.font = "100 24px Luckiest Guy";
  ctx.fillStyle = "#5aa38b";
  ctx.textAlign = "center";
  ctx.fillText(translation[langueChoisie]['miniJeu1__best'],670,45);

  ctx.font = "100 60px Luckiest Guy";
  ctx.fillStyle = "#5aa38b";
  ctx.textAlign = "center";
  ctx.fillText(bestScore,670,100);


  //si le chrono  est plus petit ou = à O, l'état du jeu passe à over.
  if (remainingTime <=0)
  {
    //si le score obtenu est supérieur au meilleur score enregistré précédement, le meilleur score devient le score obtenu
    if (score > bestScore)
    {
      stockage.setItem("monScore" , score);
      bestScore = stockage.getItem("monScore");
    }
    // on affiche le meilleur score dans la console
      console.log(bestScore);
    //état du jeu passe en over
      gameState = "over";
  }
}
// création d'une fonction qui permet de créer un compte à rebour.
function cptRebour()
{

  newTime = new Date().getTime();
  delta = newTime - lastTime;
  lastTime = newTime;

//on enleve tout ce qu'il y a dans le canvas
ctx.clearRect(0,0,900,700);

// créarion d'un switch afin de pouvoir choisir entre les différents états du jeu.
  switch (gameState)
  {
    // lorsque l'on sélectionne l'état menu, on affiche la phrase LE JEU cliquez sur espace pour commencer
    case "menu":

    ctx.drawImage(logo,240, 20);
    ctx.font = "100 23px Quicksand";
    ctx.fillStyle = "#FFFFFF";
    ctx.textAlign = "center";
    ctx.fillText(translation[langueChoisie]['miniJeu1__intro1'],canvas.width/2, 200);
    ctx.fillText(translation[langueChoisie]['miniJeu1__intro2'],canvas.width/2, 225);

  //  ctx.fillText(translation[langueChoisie]["miniJeu1__intro"],canvas.width/2, 300);


    ctx.font = "900 23px Quicksand";
    ctx.fillStyle = "#FFFFFF";
    ctx.textAlign = "center";
    ctx.fillText(translation[langueChoisie]['miniJeu1__consignes1'],canvas.width/2, 260);
    ctx.fillText(translation[langueChoisie]['miniJeu1__consignes2'],canvas.width/2, 285);


    ctx.drawImage(FG,250, 350);
    ctx.drawImage(FH,350, 300);
    ctx.drawImage(FD,450, 350);

    ctx.font = "25px Luckiest Guy";
    ctx.fillStyle = "#FFFFFF";
    ctx.textAlign = "center";
    ctx.fillText(translation[langueChoisie]['miniJeu1__start'],canvas.width/2, 490);
    break;

    // lorsque l'on sélectionne l'état game, on dessine le tableau dessin qui contient les poubelles et le déchet choisit au hasard
    case "game":
      for (var i = 0; i < objDessin.length; i++) {
        objDessin[i].dessin();
      }
      checkTime(delta);
    break;

    // lorsque l'on sélectionne l'état over, on affiche la phrase perdu recommence. ton score est de ...
    case "over":


    if (score >50) {

      ctx.textAlign = "center";
      ctx.font = "50px Luckiest Guy";
      ctx.fillStyle = "#FFFFFF";
      ctx.fillText(translation[langueChoisie]['miniJeu1__scoreNiveau3'],canvas.width/2,200);
      ctx.fillStyle = "#3457a4";
      ctx.font = "70px Luckiest Guy";
      ctx.fillText(score + " " + translation[langueChoisie]['miniJeu1__points'],canvas.width/2,280);
      ctx.fillStyle = "#FFFFFF";
      ctx.font = "50px Luckiest Guy";
      ctx.fillText(translation[langueChoisie]['miniJeu1__bestScore'],canvas.width/2,350);
      ctx.fillStyle = "#3457a4";
      ctx.font = "70px Luckiest Guy";
      ctx.fillText(bestScore + " " + translation[langueChoisie]['miniJeu1__points'],canvas.width/2,430);
      ctx.font = "20px Luckiest Guy";
      ctx.fillStyle = "#FFFFFF";
      ctx.textAlign = "center";
      ctx.fillText(translation[langueChoisie]['miniJeu1__restart'],canvas.width/2, 490);
    }
    else {
      ctx.textAlign = "center";
      ctx.font = "40px Luckiest Guy";
      ctx.fillStyle = "#FFFFFF";
      ctx.fillText(translation[langueChoisie]['miniJeu1__scoreNiveau1'],canvas.width/2,200);
      ctx.fillStyle = "#3457a4";
      ctx.font = "70px Luckiest Guy";
      ctx.fillText(score + " " + translation[langueChoisie]['miniJeu1__points'],canvas.width/2,280);
      ctx.fillStyle = "#FFFFFF";
      ctx.font = "40px Luckiest Guy";
      ctx.fillText(translation[langueChoisie]['miniJeu1__bestScore'],canvas.width/2,350);
      ctx.fillStyle = "#3457a4";
      ctx.font = "70px Luckiest Guy";
      ctx.fillText(bestScore + " " + translation[langueChoisie]['miniJeu1__points'],canvas.width/2,430);
      ctx.font = "20px Luckiest Guy";
      ctx.fillStyle = "#FFFFFF";
      ctx.textAlign = "center";
      ctx.fillText(translation[langueChoisie]['miniJeu1__restart'],canvas.width/2, 490);
    }

    break;
  default:
  }
}

var compteARebour = setInterval(cptRebour, 16);

// création d'un constructeur pour créer nos poubelles
PbConst = function(str_pbImgUrl, int_origineX, int_origineY){
  this.img = new Image();
  this.img.src = str_pbImgUrl;
  this.x = int_origineX;
  this.y = int_origineY;

  this.dessin = function (){
    ctx.drawImage(this.img, this.x, this.y);
  }
}

// création des poubelles via le constructeur
var pb1 = new PbConst('jeu/img/pbpage2-01.png',50,250);
var pb2 = new PbConst('jeu/img/pbpage2-02.png',270,50);
var pb3 = new PbConst('jeu/img/pbpage2-03.png',500,250);

// on ajoute les poubelles unes à unes aux tableaux poubelles et objDessin
//poubelles.push(pb1);
objDessin.push(pb1);

//poubelles.push(pb2);
objDessin.push(pb2);

//poubelles.push(pb3);
objDessin.push(pb3);


// création d'un tableau dans lequel on mettra les déchets
var groupeDechet = [];
var dechetSelection;
var dechetCharge = 0;

// création d'un constructeur afin de créer nos déchets
DechetConst = function(str_type,str_name,url,keyID)
{
  this.x = 290;
  this.y = 250;

  this.id = keyID;
  // création d'un objet avec une propriété spéciale qui rajoutera du temps
  this.special = false;

  this.img = new Image();
  this.img.onload= function()
  {
    dechetCharge += 1;
    if(dechetCharge == groupeDechet.length) getRandomGarbage();
  }
  this.img.src = "jeu/img/"+url;

  this.type = str_type;
  this.name = str_name;
  this.state = false;

  groupeDechet.push(this);

  this.dessin = function (){
    ctx.drawImage(this.img,this.x,this.y);
  }
}

// créarion des déchets sur base du constructeur
var dechet = new DechetConst ("pmc","pmc","blau-01.png",38);
var dechet = new DechetConst ("pmc","pmc","blau-02.png",38);
var dechet = new DechetConst ("pmc","pmc","blau-03.png",38);
var dechet = new DechetConst ("pmc","pmc","blau-04.png",38);

var dechet = new DechetConst ("carton","carton","jaune-01.png",37);
var dechet = new DechetConst ("carton","carton","jaune-02.png",37);
var dechet = new DechetConst ("carton","carton","jaune-03.png",37);


var dechet = new DechetConst ("autre","autre","autre-01.png",39);
var dechet = new DechetConst ("autre","autre","autre-02.png",39);
var dechet = new DechetConst ("autre","autre","autre-03.png",39);
var dechet = new DechetConst ("autre","autre","autre-04.png",39);




//pour un random on fait le rdm*(max-min)+min
// on ajoute un eventListener pour gérer les touches
window.addEventListener("keydown", function(event)
{
  // lorsque l'on appuie sur la touche 32 (ce qui équivaut à la barre d'espace) si l'état du jeu est soit sur menu soit sur over, on relance le jeu
  if (event.keyCode == 32)
  {
    if(gameState == "menu" || gameState == "over")
    {
      remainingTime = 30000;
      gameState = "game";
      score = 0;
    }
  }

  if(allowKey && gameState == "game")
  {
    if(event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39)
    {
          if (event.keyCode == dechetSelection.id)
          {
            score += 10;
            if(dechetSelection.special)
            {
              remainingTime += 10000;
              dechetSelection.special = false;
            }

          }
          else {
            if(score > 0) score -= 5;
            remainingTime -= 3000;

          }
          console.log(score);
          allowKey = false;

          objDessin.splice(3,1);
          getRandomGarbage();
    }
  }
});

function getRandomGarbage()
{
  var hasard = Math.floor(Math.random()*groupeDechet.length);
  objDessin.push(groupeDechet[hasard]);
  dechetSelection = groupeDechet[hasard];

  var randomSpecial = Math.floor(Math.random()*20);
  if(randomSpecial == 1)
  {
    groupeDechet[hasard].special = true;
  }

  allowKey = true;
}
