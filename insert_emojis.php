
<?php
try {
  // on relie la db config pour avoir acces au dbname, au password et au user
  include 'db_config.php';
  // on crée une variable data base host dans laquelle on stock le PDO (interface pour accéder à la base de donnée déclarée entre parenthèse)
  $dbh = new PDO ('mysql:host=localhost;dbname='.$dbname,$user,$password);
  // on crée une variable statement dans laquelle on prépare une requete SQL (ce que l'on va envoyer dans la base de donnée) avec des placeholder qui serviront de sécurité
  $stmt = $dbh->prepare("INSERT INTO `feedback` (`id`, `date`, `gradation`) VALUES (NULL, CURRENT_TIMESTAMP, :gradation)");
  // on remplace le placeholder par la variable
  $stmt->bindParam(":gradation", $_REQUEST['grade']);
  // on exécute
  $stmt->execute();
  echo "succes";
} catch (PDOException $e) {
  echo "error";
}
