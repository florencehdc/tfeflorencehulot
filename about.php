<!DOCTYPE html>
<html>

  <head>

  	<meta charset="utf-8">

  	<title>Ekool</title>

    <!-- Jquery -->
    <script src="/js/librairies/jquery-3.2.1.min.js"></script>
    <script src ="/js/librairies/js.cookie.js"></script>

    <!-- GSAP -->
      <!-- TweenMax -->
      <script src="/js/librairies/GSAP/TweenMax.min.js"></script>
      <!-- GSAP plugins -->
        <!-- ScrollMagic -->
        <script src="/js/librairies/GSAP/plugins/ScrollMagic/ScrollMagic.min.js"></script>
          <!-- ScrollMagic plugins -->
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/animation.gsap.min.js"></script>
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/animation.velocity.min.js"></script>
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/debug.addIndicators.min.js"></script>
          <script src="/js/librairies/GSAP/plugins/ScrollMagic/plugins/jquery.ScrollMagic.min.js"></script>
        <!-- ScrollToPlugin -->
        <script src="/js/librairies/GSAP/plugins/ScrollToPlugin.min.js"></script>

    <!-- CSS Style-->
    <link href="/css/screen.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,|Quicksand:300,400,500,700,|Luckiest+Guy:400" rel="stylesheet">

  </head>

    <!-- JS -->
      <!-- Animations -->
      <script src="/js/about/about.js"></script>
      <!-- JS all scripts -->
      <script src="/js/master.js"></script>

    <body>

      <!-- Audio -->

      <audio id="audioPlayer_about">
        <source src="/audio/home2.ogg">
        <source src="/audio/home2.mp3">
      </audio>
      <button type="button" id="mute3" name="mute3"></button>

      <!-- Alerte 30 min d'utilisation -->

      <div class="alert-tps alert--hide">
        <div class="alert-tps__container">
          <div class="alert-tps__message">
            <p> Attention, tu es devant ton écran depuis 30 minutes.
                <br>
                <br> Waarschuwing, u bent op de computer  gedurende 30 minuten.
            </p>
          </div>
          <div class="alert-tps__boutons">
            <p><button type="button" name="button" class="closeAlert"> OK </button></p>
            <!--<p><a href="http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm" target="_blank"><button type="button" name="button" class="closeMessage"> En savoir plus </button></a></p>-->
          </div>
        </div>
        <div class="alert-tps__cache"></div>
      </div>

      <div class="page about">

        <div class="scrollDown" id="scrollDown">
          <p>scroll<br><br><img src="/img/scroll_about_homepage.svg" alt="scroll down"></p>
        </div>

        <div class="backindex--about" id="backhomepage">
          <a href="index.php">
            <img src="/img/retour_menu_about.svg" alt="back homepage">
          </a>
        </div>

        <div class="about__introduction" id="trigger--introduction">
          <div class="about__vaisceau-vide">
            <img id="vaisceau-vide" src="/img/about/background/vaisceau_vide.svg">
          </div>
          <div class="about__meteorites">
            <div class="meteorite--1" id="meteorite--1">
              <img src="/img/about/background/meteorite_1.svg">
            </div>
            <div class="meteorite--2" id="meteorite--2">
              <img src="/img/about/background/meteorite_2.svg">
            </div>
            <div class="meteorite--3" id="meteorite--3">
              <img src="/img/about/background/meteorite_3.svg">
            </div>
            <div class="meteorite--4" id="meteorite--4">
              <img src="/img/about/background/meteorite_1.svg">
            </div>
            <div class="meteorite--5" id="meteorite--5">
              <img src="/img/about/background/meteorite_2.svg">
            </div>
            <div class="meteorite--6" id="meteorite--6">
              <img src="/img/about/background/meteorite_1.svg">
            </div>
            <div class="meteorite--7" id="meteorite--7">
              <img src="/img/about/background/meteorite_4.svg">
            </div>
            <div class="meteorite--8" id="meteorite--8">
              <img src="/img/about/background/meteorite_3.svg">
            </div>
            <div class="meteorite--9" id="meteorite--9">
              <img src="/img/about/background/meteorite_2.svg">
            </div>
            <div class="meteorite--10" id="meteorite--10">
              <img src="/img/about/background/meteorite_1.svg">
            </div>
            <div class="meteorite--11" id="meteorite--11">
              <img src="/img/about/background/meteorite_1.svg">
            </div>
            <div class="meteorite--12" id="meteorite--12">
              <img src="/img/about/background/meteorite_2.svg">
            </div>
            <div class="meteorite--13" id="meteorite--13">
              <img src="/img/about/background/meteorite_3.svg">
            </div>
            <div class="meteorite--14" id="meteorite--14">
              <img src="/img/about/background/meteorite_1.svg">
            </div>
            <div class="meteorite--15" id="meteorite--15">
              <img src="/img/about/background/meteorite_2.svg">
            </div>
            <div class="meteorite--16" id="meteorite--16">
              <img src="/img/about/background/meteorite_4.svg">
            </div>
            <div class="meteorite--17" id="meteorite--17">
              <img src="/img/about/background/meteorite_1.svg">
            </div>
            <div class="meteorite--18" id="meteorite--18">
              <img src="/img/about/background/meteorite_3.svg">
            </div>
            <div class="meteorite--19" id="meteorite--19">
              <img src="/img/about/background/meteorite_1.svg">
            </div>
            <div class="meteorite--20" id="meteorite--20">
              <img src="/img/about/background/meteorite_2.svg">
            </div>
          </div>
          <p class= "introduction__title" data-t-id="introduction__title"> À propos </p>
          <div class="introduction__logo" id="trigger2--introduction">
            <img src="/img/about/LOGO/logo.svg" alt="Ekool">
          </div>
          <div class="about__nuage nuage--1" id="nuage--1">
            <img src="/img/about/background/clouds_2.svg">
          </div>
          <div class="about__planets">
            <div class="planet--1 js-rotate" id="planet--1">
              <img src="/img/about/background/plastika.svg" alt="plastika">
            </div>
            <div class="planet--2 js-rotate" id="planet--2">
              <img src="/img/about/background/elektra.svg" alt="elektra">
            </div>
            <div class="planet--3 js-rotate" id="planet--3">
              <img src="/img/about/background/organika.svg" alt="organika">
            </div>
            <div class="planet--4 js-rotate" id="planet--4">
              <img src="/img/about/background/brolaka.svg" alt="brolaka">
            </div>
            <div class="planet--5 js-rotate" id="planet--5">
              <img src="/img/about/background/verraka.svg" alt="verraka">
            </div>
            <div class="planet--6 js-rotate" id="planet--6">
              <img src="/img/about/background/kapapika.svg" alt="kapapika">
            </div>
          </div>
          <div class="about__stars--1">
            <div class="star--1" id="star--1">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--2" id="star--2">
              <img src="/img/about/background/stars_1.png">
            </div>
            <div class="star--3" id="star--3">
              <img src="/img/about/background/stars_1.png">
            </div>
            <div class="star--4" id="star--4">
              <img src="/img/about/background/stars_3.png">
            </div>
            <div class="star--5" id="star--5">
              <img src="/img/about/background/stars_3.png">
            </div>
            <div class="star--6" id="star--6">
              <img src="/img/about/background/stars_3.png">
            </div>
            <div class="star--7" id="star--7">
              <img src="/img/about/background/stars_1.png">
            </div>
            <div class="star--8" id="star--8">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--9" id="star--9">
              <img src="/img/about/background/stars_3.png">
            </div>
            <div class="star--10" id="star--1">
              <img src="/img/about/background/stars_1.png">
            </div>
            <div class="star--11" id="star--2">
              <img src="/img/about/background/stars_1.png">
            </div>
            <div class="star--12" id="star--3">
              <img src="/img/about/background/stars_3.png">
            </div>
            <div class="star--13" id="star--4">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--14" id="star--5">
              <img src="/img/about/background/stars_3.png">
            </div>
            <div class="star--15" id="star--6">
              <img src="/img/about/background/stars_1.png">
            </div>
            <div class="star--16" id="star--7">
              <img src="/img/about/background/stars_1.png">
            </div>
            <div class="star--17" id="star--8">
              <img src="/img/about/background/stars_1.png">
            </div>
          </div>
        </div>

        <div class="module-interstitiel"></div>

        <div class="nouveau-module brain" id="trigger--brain">
          <div class="about__nuage nuage--2" id="nuage--2">
            <img src="/img/about/background/clouds_1.svg">
          </div>
          <div class="brain__title">
            <p data-t-id="brain__title"> Les cerveaux </p>
          </div>
          <div class="brain__illu">
            <div class="cerveau-a">
              <img class="cerveau1-a" id="cerveau1-a" src="/img/about/modules/cerveau_small1_a.svg">
              <img class="cerveau2-a" id="cerveau2-a" src="/img/about/modules/cerveau_small2_a.svg">
              <img class="cerveau3-a" id="cerveau3-a" src="/img/about/modules/cerveau_small3_a.svg">
              <img class="cerveau4-a" id="cerveau4-a" src="/img/about/modules/cerveau_small4_a.svg">
              <img class="cerveau5-a" id="cerveau5-a" src="/img/about/modules/cerveau_small5_a.svg">
              <img class="cerveau6-a" id="cerveau6-a" src="/img/about/modules/cerveau_small6_a.svg">
              <img class="cerveau7-a" id="cerveau7-a" src="/img/about/modules/cerveau_small7_a.svg">
              <img class="cerveau8-a" id="cerveau8-a" src="/img/about/modules/cerveau_small8_a.svg">
              <img class="yeux-a" id="yeux-a" src="/img/about/modules/yeux_small_a.svg">
            </div>
            <div class="bombonne-a" id="bombonne-a">
              <img src="/img/about/modules/bombonne_small_a.svg">
            </div>
            <div class="cerveau-b">
              <img class="cerveau1-b" id="cerveau1-b" src="/img/about/modules/cerveau_small1_b.svg">
              <img class="cerveau2-b" id="cerveau2-b" src="/img/about/modules/cerveau_small2_b.svg">
              <img class="cerveau3-b" id="cerveau3-b" src="/img/about/modules/cerveau_small3_b.svg">
              <img class="cerveau4-b" id="cerveau4-b" src="/img/about/modules/cerveau_small4_b.svg">
              <img class="cerveau5-b" id="cerveau5-b" src="/img/about/modules/cerveau_small5_b.svg">
              <img class="cerveau6-b" id="cerveau6-b" src="/img/about/modules/cerveau_small6_b.svg">
              <img class="cerveau7-b" id="cerveau7-b" src="/img/about/modules/cerveau_small7_a.svg">
              <img class="cerveau8-b" id="cerveau8-b" src="/img/about/modules/cerveau_small8_a.svg">
              <img class="yeux-b" id="yeux-b" src="/img/about/modules/yeux_small_b.svg">
            </div>
            <div class="bombonne-b" id="bombonne-b">
              <img src="/img/about/modules/bombonne_small_b.svg">
            </div>
          </div>
          <div class="about__nuage nuage--3" id="nuage--3">
            <img src="/img/about/background/clouds_3.svg">
          </div>
          <div class="brain__body text-align--center" id="trigger--vaisceau">
            <p data-t-id="brain__body"> Nous nous appelons Florence et Alice. Nous avons respectivement 25 et 21 ans et nous sommes toutes deux étudiantes en Infographie et Multimédia en troisième et dernière année à l'Institut des Arts de Diffusion.  </p>
          </div>
          <div class="about__stars--2">
            <div class="star--18" id="star--10">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--19" id="star--11">
              <img src="/img/about/background/stars_1.png">
            </div>
            <div class="star--20" id="star--12">
              <img src="/img/about/background/stars_1.png">
            </div>
            <div class="star--21" id="star--13">
              <img src="/img/about/background/stars_3.png">
            </div>
            <div class="star--22" id="star--14">
              <img src="/img/about/background/stars_3.png">
            </div>
            <div class="star--23" id="star--15">
              <img src="/img/about/background/stars_3.png">
            </div>
            <div class="star--24" id="star--16">
              <img src="/img/about/background/stars_1.png">
            </div>
            <div class="star--25" id="star--17">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--26" id="star--18">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--27" id="star--18">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--28" id="star--18">
              <img src="/img/about/background/stars_2.png">
            </div><div class="star--25" id="star--17">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--29" id="star--18">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--30" id="star--18">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--31" id="star--18">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--32" id="star--18">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--33" id="star--18">
              <img src="/img/about/background/stars_2.png">
            </div>
            <div class="star--34" id="star--18">
              <img src="/img/about/background/stars_2.png">
            </div>
          </div>
          <div class="about__vaisceau-rempli">
            <img id="vaisceau-rempli" src="/img/about/background/vaisceau_rempli.svg">
          </div>
        </div>


        <div class="nouveau-module project" id="trigger--project">
          <div class="project__title">
            <p data-t-id="project__title"> Le projet </p>
          </div>
          <div class="project__body text-align--justify">
            <ul>
              <li>
                <p data-t-id="project__body--1" id="trigger--vaisceau2">
                Ce projet a été réalisé dans le cadre de notre travail de fin d'études. Nous voulions réaliser une plateforme éducative et ludique destinée à un jeune public.
                Le choix du thème a été orienté par un désir de traiter un sujet qui de prime abord n'intéresse pas ou n'est pas suffisamment expliqué aux enfants de 8 à 10 ans.
                <br>
                <br>Après avoir posé la question à quelques jeunes de cette tranche d'âge pour savoir quel sujet ils aimeraient nous voir traiter pour un tel projet, nous avons constaté que la question du recyclage était encore floue et incomprise pour un grand nombre d'entre eux mais qu'il étaient désireux d'en savoir plus !
              </p>
            </li>
              <li>
                <p data-t-id="project__body--2" id="trigger--vaisceau2">
                  <br>Nous nous sommes donc finalement orientées vers ce thème. Il nous a ensuite paru important d'inculquer, dès le plus jeune âge, les bons réflexes à adopter pour préserver notre planète ; et surtout d'en expliquer les raisons de la manière la plus attractive possible.
                  <br>
                  <br>Afin de mieux comprendre notre public cible et d'en déterminer correctement les besoins et les attentes, nous avons travaillé en collaboration avec Mme Stéphanie Letto, institutrice à l'Athénée Royal Riva-Bella de Braine-l'Alleud. Nous la remercions d'ail- leurs chaleureusement pour son aide !
                </p>
              </li>
            </ul>
          </div>
          <div class="about__nuage nuage--5" id="nuage--5">
            <img src="/img/about/background/clouds_1.svg">
          </div>
          <div class="about__vaisceau-vide2">
            <img id="vaisceau-vide2" src="/img/about/background/vaisceau_vide2.svg">
          </div>
        </div>

        <div class="module-interstitiel">
          <svg>

          </svg>
        </div>

        <div class="nouveau-module contact" id="trigger--contact">
          <div class="about__nuage nuage--4" id="nuage--4">
            <img src="/img/about/background/clouds_4.svg">
          </div>
          <div class="contact__texte">
            <div class="contact__title">
              <p data-t-id="contact__title"> Contact </p>
            </div>
            <div class="contact__body">
              <p data-t-id="contact__body">
                Envie de collaborer ? Des questions ? N'hésitez pas à nous contacter : <strong>ekool@gmail.com</strong>
                <br>
                <br>
                <br>
                Ou via les réseaux sociaux :
              </p>
              <ul>
                <li>
                  <a href="https://www.facebook.com/" class="facebook">Facebook</a>
                </li>
                <li>
                  <a href="https://www.instagram.com/" class="instagram">Instagram</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="contact__illu">
            <!--<img src="/img/about/modules/illu_about.jpg" alt="illu">-->
          </div>
        </div>

        <div class="footer--about">
          <div class="footer__copyright--about">
            <p data-t-id="footer__copyright">© 2017 Ekool - Tous droits réservés</p>
          </div>
        </div>

      </div>
    </body>

</html>
