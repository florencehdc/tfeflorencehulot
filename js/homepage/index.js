window.addEventListener("load", function(){

/* AUDIO */

  var player = document.querySelector('#audioPlayer');
  player.play();
  player.loop = true;

  function mute(){
    if (player.volume == 0){
      player.volume = 1;
      document.getElementById("notMute").id = "mute";
    }else {
      player.volume=0;
      document.getElementById("mute").id = "notMute";
    }
  }
  function boutonMute()
  {
    var muteSound = document.getElementById("mute");
    muteSound.addEventListener("click", mute);
  }
  boutonMute();


/* ANIMATIONS FIXED */

  /* planète */
  var js_rotate = document.getElementsByClassName("js-rotate");
  TweenMax.to(js_rotate, 200, {rotation:-360, repeat:-1, ease: Power0.easeNone});

  /* fusée */
  var js_rotate__inverse = document.getElementsByClassName("js-rotate--inverse");
  TweenMax.fromTo(js_rotate__inverse, 20, {y:22, x:0},{rotation:360, transformOrigin : "170px 170px", repeat:-1, ease: Power0.easeNone});

/* ANIMATIONS ON SCROLL */

    // initialisation du controlleur
    var controller = new ScrollMagic.Controller();

    /* PLANET Kapapika */
      // construction du tween
      var kapapika = TweenMax.to('#planet1', 1, {x:'30'});
      // construction de la scène
      var s_kapapika = new ScrollMagic.Scene({
        triggerElement: "#planet1",
        triggerHook: 0.2
      })
      .setTween(kapapika)
      .addTo(controller);

    /* PLANET Plastika */
      var plastika = TweenMax.to('#planet2', 1, {x:'-30'});
      var s_plastika = new ScrollMagic.Scene({
        triggerElement: "#planet2",
        triggerHook: 0.2
      })
      .setTween(plastika)
      .addTo(controller);

    /* PLANET Brolaka */
      var brolaka = TweenMax.to('#planet3', 1, {x:'30'});
      var s_brolaka = new ScrollMagic.Scene({
        triggerElement: "#planet3",
        triggerHook: 0.2
      })
      .setTween(brolaka)
      .addTo(controller);

    /* PLANET Verraka */
      // construction du tween
      var verraka = TweenMax.to('#planet4', 1, {x:'-30'});
      // construction de la scène
      var s_verraka = new ScrollMagic.Scene({
        triggerElement: "#planet4",
        triggerHook: 0.2
      })
      .setTween(verraka)
      .addTo(controller);

    /* PLANET Organika */
      var organika = TweenMax.to('#planet5', 1, {x:'30'});
      var s_organika = new ScrollMagic.Scene({
        triggerElement: "#planet5",
        triggerHook: 0.2
      })
      .setTween(organika)
      .addTo(controller);

    /* PLANET Elektra */
      var elektra = TweenMax.to('#planet6', 1, {x:'-30'});
      // construction de la scène
      var s_elektra = new ScrollMagic.Scene({
        triggerElement: "#planet6",
        triggerHook: 0.2
      })
      .setTween(elektra)
      .addTo(controller);

    /* NUAGE 1 */
      var clouds1 = TweenMax.to('#clouds1', 1, {y:'100'});
      // construction de la scène
      var s_clouds1 = new ScrollMagic.Scene({
        triggerElement: "#clouds1",
        triggerHook: 0.6,
        duration: 600
      })
      .setTween(clouds1)
      .addTo(controller);

    /* NUAGE 2 */
      var clouds2 = TweenMax.to('#clouds2', 1, {y:'-100'});
      // construction de la scène
      var s_clouds2 = new ScrollMagic.Scene({
        triggerElement: "#clouds2",
        triggerHook: 0.6,
        duration: 600
      })
      .setTween(clouds1)
      .addTo(controller);

    /* NUAGE 3 */
      var clouds3 = TweenMax.to('#clouds3', 1, {y:'-100'});
      // construction de la scène
      var s_clouds3 = new ScrollMagic.Scene({
        triggerElement: "#clouds3",
        triggerHook: 0.6,
        duration: 600
      })
      .setTween(clouds3)
      .addTo(controller);

    /* NUAGE 4 */
      var clouds4 = TweenMax.to('#clouds4', 1, {y:'-100'});
      // construction de la scène
      var s_clouds4 = new ScrollMagic.Scene({
        triggerElement: "#clouds4",
        triggerHook: 0.6,
        duration: 600
      })
      .setTween(clouds4)
      .addTo(controller);

    /* NUAGE 5 */
      var clouds5 = TweenMax.to('#clouds5', 1, {y:'-100'});
      // construction de la scène
      var s_clouds5 = new ScrollMagic.Scene({
        triggerElement: "#clouds5",
        triggerHook: 0.6,
        duration: 600
      })
      .setTween(clouds5)
      .addTo(controller);

    /* NUAGE 6 */
      var clouds6 = TweenMax.to('#clouds6', 1, {y:'-100'});
      // construction de la scène
      var s_clouds6 = new ScrollMagic.Scene({
        triggerElement: "#clouds6",
        triggerHook: 0.6,
        duration: 600
      })
      .setTween(clouds6)
      .addTo(controller);

    /* NUAGE 7 */
      var clouds7 = TweenMax.to('#clouds7', 1, {y:'-100'});
      // construction de la scène
      var s_clouds7 = new ScrollMagic.Scene({
        triggerElement: "#clouds7",
        triggerHook: 0.6,
        duration: 600
      })
      .setTween(clouds7)
      .addTo(controller);

    /* NUAGE 8 */
      var clouds8 = TweenMax.to('#clouds8', 1, {y:'-100'});
      // construction de la scène
      var s_clouds8 = new ScrollMagic.Scene({
        triggerElement: "#clouds8",
        triggerHook: 0.6,
        duration: 600
      })
      .setTween(clouds8)
      .addTo(controller);

     /* ETOILES PLANET KAPAPIKA */
     var stars__planet1 = TweenMax.from('#stars__planet1', 1, {opacity:0.5});
     // construction de la scène
     var s_stars__planet1 = new ScrollMagic.Scene({
       triggerElement: "#planet1",
       triggerHook: 0.5
     })
     .setTween(stars__planet1)
     .addTo(controller);

     /* ETOILES PLANET PLASTIKA */
     var stars__planet2 = TweenMax.from('#stars__planet2', 1, {opacity:0.5});
     // construction de la scène
     var s_stars__planet2 = new ScrollMagic.Scene({
       triggerElement: "#planet2",
       triggerHook: 0.5
     })
     .setTween(stars__planet2)
     .addTo(controller);

     /* ETOILES PLANET BROLAKA */
     var stars__planet3 = TweenMax.from('#stars__planet3', 1, {opacity:0.5});
     // construction de la scène
     var s_stars__planet3 = new ScrollMagic.Scene({
       triggerElement: "#planet3",
       triggerHook: 0.5
     })
     .setTween(stars__planet3)
     .addTo(controller);

     /* ETOILES PLANET VERRAKA */
     var stars__planet4 = TweenMax.from('#stars__planet4', 1, {opacity:0.5});
     // construction de la scène
     var s_stars__planet4 = new ScrollMagic.Scene({
       triggerElement: "#planet4",
       triggerHook: 0.5
     })
     .setTween(stars__planet4)
     .addTo(controller);

     /* ETOILES PLANET ORGANIKA */
     var stars__planet5 = TweenMax.from('#stars__planet5', 1, {opacity:0.5});
     // construction de la scène
     var s_stars__planet5 = new ScrollMagic.Scene({
       triggerElement: "#planet5",
       triggerHook: 0.5
     })
     .setTween(stars__planet5)
     .addTo(controller);

     /* ETOILES PLANET ELEKTRA */
     var stars__planet6 = TweenMax.from('#stars__planet6', 1, {opacity:0.5});
     // construction de la scène
     var s_stars__planet6 = new ScrollMagic.Scene({
       triggerElement: "#planet6",
       triggerHook: 0.5
     })
     .setTween(stars__planet6)
     .addTo(controller);


});
