window.addEventListener("load", function()
{

  //LOADER
  var index = document.getElementsByClassName("index"); /* homepage */
  var alertCookies = document.getElementsByClassName("alert-cookies");
  var alertCookies__container = document.getElementsByClassName("alert-cookies__container"); /* alert cookies */
  var alertCookies__cache = document.getElementsByClassName("alert-cookies__cache"); /* cache homepage */
  var popup = document.getElementsByClassName("popup");
  var tl = new TimelineLite(); // ici je crée une timeline pour que les différents éléments de base de la page apparaissent progressivement
  tl.fromTo(index, 3, {autoAlpha:0},{autoAlpha:1});
  console.log("Bienvenue sur Ekool"); // je vérifie que la fonction s'exécute correctement


  /*##################################*/
  /* = POPUP UTILISATION DE COOKIES !
  ####################################*/

  // si le cookie est déjà apparu
  if (Cookies.get('alert-cookies') == 'yes') {
    $('.alert-cookies').removeClass('alert--show');
    $('.alert-cookies').addClass('alert--hide');
  }
  // si non
  else { // si le cookie n'est pas encore apparu
    $('.alert-cookies').removeClass('alert--hide');
    $('.alert-cookies').addClass('alert--show');

    // j'enclenche l'anim d'apparition
    tl.from(alertCookies__container, 2, {y:-200}, '-= 0.7');
    tl.fromTo(alertCookies__cache, 2, {autoAlpha:0},{autoAlpha:0.5}, '-= 0.7');

    // je désactive le scroll de la page
    window.disableScroll = function(e){
        if (e.keyCode) {
          /^(32|33|34|35|36|38|40)$/.test(e.keyCode) && e.preventDefault();
        }
        else {
          e.preventDefault();
        }
      }
      addEventListener("mousewheel", disableScroll, false);
      addEventListener("DOMMouseScroll", disableScroll, false);
      addEventListener("keydown", disableScroll, false);
  }

  $('.closeAlert').click(function() {

    if (!$('.alert-cookies').is('alert--show')) {

      // j'enclenche l'anim de disparition
      tl.fromTo(alertCookies__cache, 2, {autoAlpha:0.5},{autoAlpha:0});
      tl.to(alertCookies__container, 2, {y:-200}, '-= 0.7');
      tl.to(alertCookies, 0.2, {y:-1000});

      Cookies.set('alert-cookies', 'yes'); // j'enregistre la valeur de change-state--on-click dans un cookie valable sur le'entièreté du site'
    }

    return false;
  });


  /*##################################*/
  /* = POPUP MODAL !
  ####################################*/

  //##### TEST AFFICHAGE #####//

  // si le cookie est déjà apparu
  if (Cookies.get('change-state--on-click') == 'yes') { // si le cookie est déjà apparu et que donc la valeur enregistrée est la classe "show"
    $('.change-state--on-click').removeClass('popup--show'); // alors je remove la classe "show"
    $('.change-state--on-click').addClass('popup--hide'); // et je la remplace par la classe "hide" pour ne plus voir apparaître le popup
  }
  // si non
  else { // si le cookie n'est pas encore apparu
    $('.change-state--on-click').removeClass('popup--hide'); // alors je remove la classe "hide"
    $('.change-state--on-click').addClass('popup--show'); // et je la remplace par la classe "show" pour voir apparaître le popup
  }

  $('.close').click(function() { // je crée une fonction qui se déclenchera en cliquant sur la croix de la modal
    if (!$('.change-state--on-click').is('popup--hide')) { // la modal est apparente (classe "show") et je veux la cacher (classe "hide") en cliquant sur la croix
      tl.to(popup, 2, {y:-1000});

      // je réactive le scroll de la page
      removeEventListener("mousewheel", disableScroll);
      removeEventListener("DOMMouseScroll", disableScroll);
      removeEventListener("keydown", disableScroll);

      Cookies.set('change-state--on-click', 'yes'); // j'enregistre la valeur de change-state--on-click dans un cookie valable sur le'entièreté du site'
    }
    return false;
  });


  //##### VISUEL BOUTONS #####//

  var $languageDefault = document.getElementById('FR');
  $($languageDefault).removeClass('languages--inactive')
                     .addClass('languages--active');

  /* BTN GROUPE */
  var $buttonGroup = $('.languages__list .languages__item button');

  $buttonGroup.click(function() {

    var button = this;
    console.log(button);

    $buttonGroup.removeClass('languages--active');

    $(this).removeClass('languages--inactive');
    $(this).addClass('languages--active');

    Cookies.set('selected_visual', button.getAttribute('data-button-value'));

    return false;

  });

  if(Cookies.get('selected_visual') !== undefined)
  {
    var currentVisual = document.querySelector('button[data-button-value="'+Cookies.get('selected_visual')+'"]');
    console.log(currentVisual);
    $(currentVisual).removeClass('languages--inactive');
    $(currentVisual).addClass('languages--active');

    if(Cookies.get('selected_visual') !== 'fr') {
      $($languageDefault).removeClass('languages--active')
                         .addClass('languages--inactive');
    }
  }

});
