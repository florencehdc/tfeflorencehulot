/* STRUCTURE

    BIBLIOTHÈQUE -- ANIMATIONS JS
    Sont répertoriées ici l'ensemble des animations JS GSAP disponibles et applicables à tout
    élément de la plateforme en ajoutant tout simplement la classe correspondant à l'animation
    désirée à ce dernier (dans le fichier HTML).

    SCROLL DOWN

    LOADER LANGUAGES

    BIBLIOTHÈQUE -- TRADUCTIONS (POPUP/INDEX)
    Les variables chargées varient en fonction de la langue choisie.

*/


window.onload = function() {


/*##################################*/
/* = ANIMATION
####################################*/

    // comein

       /* fast */
        //from left
        var comein_fromleft_fast = document.getElementsByClassName("js-anim-comein--fromleft--fast");
        TweenMax.fromTo(comein_fromleft_fast, 1, {x: -800}, {x: 2000});
        //from right
        var comein_fromright_fast = document.getElementsByClassName("js-anim-comein--fromright--fast");
        TweenMax.fromTo(comein_fromright_fast, 1, {x: 2000}, {x: -800});
        //from top
        var comein_fromtop_fast = document.getElementsByClassName("js-anim-comein--fromtop--fast");
        TweenMax.fromTo(comein_fromtop_fast, 1, {y: 2000}, {y: -800});
        //from bottom
        var comein_frombottom_fast = document.getElementsByClassName("js-anim-comein--frombottom--fast");
        TweenMax.fromTo(comein_frombottom_fast, 1, {y: -800}, {y: 2000});

       /* slow */
        //from left
        var comein_fromleft_slow = document.getElementsByClassName("js-anim-comein--fromleft--slow");
        TweenMax.fromTo(comein_fromleft_slow, 5, {x: -800}, {x: 2000});
        //from right
        var comein_fromright_slow = document.getElementsByClassName("js-anim-comein--fromright--slow");
        TweenMax.fromTo(comein_fromright_slow, 5, {x: 2000}, {x: -800});
        //from top
        var comein_fromtop_slow = document.getElementsByClassName("js-anim-comein--fromtop--slow");
        TweenMax.fromTo(comein_fromtop_slow, 5, {y: 2000}, {y: -800});
        //from bottom
        var comein_frombottom_slow = document.getElementsByClassName("js-anim-comein--frombottom--slow");
        TweenMax.fromTo(comein_frombottom_slow, 5, {y: -800}, {y: 2000});

    // comeout

        /* fast */
         //to left
         var comeout_toleft_fast = document.getElementsByClassName("js-anim-comeout--toleft--fast");
         TweenMax.fromTo(comeout_toleft_fast, 1, {x: 800}, {x: -2000});
         //to right
         var comeout_toright_fast = document.getElementsByClassName("js-anim-comeout--toright--fast");
         TweenMax.fromTo(comeout_toright_fast, 1, {x: 800}, {x: 2000});
         //to top
         var comeout_totop_fast = document.getElementsByClassName("js-anim-comeout--totop--fast");
         TweenMax.fromTo(comeout_totop_fast, 1, {y: 800}, {y: 2000});
         //to bottom
         var comeout_tobottom_fast = document.getElementsByClassName("js-anim-comeout--tobottom--fast");
         TweenMax.fromTo(comeout_tobottom_fast, 1, {y: 800}, {y: -2000});

       /* slow */
        //to left
        var comeout_toleft_slow = document.getElementsByClassName("js-anim-comeout--toleft--slow");
        TweenMax.fromTo(comeout_toleft_slow, 5, {x: 800}, {x: -2000});
        //to right
        var comeout_toright_slow = document.getElementsByClassName("js-anim-comeout--toright--slow");
        TweenMax.fromTo(comeout_toright_slow, 5, {x: 800}, {x: 2000});
        //to top
        var comeout_totop_slow = document.getElementsByClassName("js-anim-comeout--totop--slow");
        TweenMax.fromTo(comeout_totop_slow, 5, {y: 800}, {y: 2000});
        //to bottom
        var comeout_tobottom_slow = document.getElementsByClassName("js-anim-comeout--tobottom--slow");
        TweenMax.fromTo(comeout_tobottom_slow, 5, {y: 800}, {y: -2000});

    // rotate

       /* fast */
        //90
        var rotate_90more_fast = document.getElementsByClassName("js-anim-rotate--90more--fast");
        TweenMax.fromTo(rotate_90more_fast, 1, {rotation:-90}, {rotation:0});
        //-90
        var rotate_90less_fast = document.getElementsByClassName("js-anim-rotate--90less--fast");
        TweenMax.fromTo(rotate_90less_fast, 1, {rotation:90}, {rotation:0});
        //180
        var rotate_180more_fast = document.getElementsByClassName("js-anim-rotate--180more--fast");
        TweenMax.fromTo(rotate_180more_fast, 1, {rotation:-180}, {rotation:0});
        //-180
        var rotate_180less_fast = document.getElementsByClassName("js-anim-rotate--180less--fast");
        TweenMax.fromTo(rotate_180less_fast, 1, {rotation:180}, {rotation: 0});
        //360
        var rotate_360more_fast = document.getElementsByClassName("js-anim-rotate--360more--fast");
        TweenMax.fromTo(rotate_360more_fast, 1, {rotation:-360}, {rotation:0});
        //-360
        var rotate_360less_fast = document.getElementsByClassName("js-anim-rotate--360less--fast");
        TweenMax.fromTo(rotate_360less_fast, 1, {rotation:360}, {rotation: 0});

       /* slow */
        //90
        var rotate_90more_slow = document.getElementsByClassName("js-anim-rotate--90more--slow");
        TweenMax.fromTo(rotate_90more_slow, 1, {rotation:-90}, {rotation:0});
        //-90
        var rotate_90less_slow = document.getElementsByClassName("js-anim-rotate--90less--slow");
        TweenMax.fromTo(rotate_90less_slow, 1, {rotation:90}, {rotation:0});
        //180
        var rotate_180more_slow = document.getElementsByClassName("js-anim-rotate--180more--slow");
        TweenMax.fromTo(rotate_180more_slow, 1, {rotation:-180}, {rotation:0});
        //-180
        var rotate_180less_slow = document.getElementsByClassName("js-anim-rotate--180less--slow");
        TweenMax.fromTo(rotate_180less_slow, 1, {rotation:180}, {rotation: 0});
        //360
        var rotate_360more_slow = document.getElementsByClassName("js-anim-rotate--360more--slow");
        TweenMax.fromTo(rotate_360more_slow, 1, {rotation:-360}, {rotation:0});
        //-360
        var rotate_360less_slow = document.getElementsByClassName("js-anim-rotate--360less--slow");
        TweenMax.fromTo(rotate_360less_slow, 1, {rotation:360}, {rotation: 0});

    // scale

       /* fast */
        //grow up
        var scale_growup_fast = document.getElementsByClassName("js-anim-scale--growup--fast");
        TweenMax.fromTo(scale_growup_fast, 1, {scale:0}, {scale:1});
        //decrease
        var scale_decrease_fast = document.getElementsByClassName("js-anim-scale--decrease--fast");
        TweenMax.fromTo(scale_decrease_fast, 1, {scale:1}, {scale:0});
       /* slow */
        //grow up
        var scale_growup_slow = document.getElementsByClassName("js-anim-scale--growup--slow");
        TweenMax.fromTo(scale_growup_slow, 5, {scale:0}, {scale:1});
        //decrease
        var scale_decrease_slow = document.getElementsByClassName("js-anim-scale--decrease--slow");
        TweenMax.fromTo(scale_decrease_slow, 5, {scale:1}, {scale:0});

    /*##################################*/
    /* = DURATION
    ####################################*/

    // appear
        /* fast */
        var appear_fast = document.getElementsByClassName("js-anim-appear--fast");
        TweenMax.fromTo(appear_fast, 1, {autoAlpha: 0}, {autoAlpha: 1});
        /* slow */
        var appear_slow = document.getElementsByClassName("js-anim-appear--slow");
        TweenMax.fromTo(appear_slow, 5, {autoAlpha: 0}, {autoAlpha: 1});

    /*##################################*/
    /* = REPEAT
    ####################################*/

    // repeat
       /* fast */
          //define : 2
          var repeat_twice_fast = document.getElementsByClassName("js-anim-repeat--twice--fast");
          TweenMax.to(repeat_twice_fast, 1, {repeat: 2});
          //infinite loop : -1
          var repeat_infinite_fast = document.getElementsByClassName("js-anim-repeat--inifinite--fast");
          TweenMax.to(repeat_infinite_fast, 1, {repeat: -1});
       /* slow */
         //define : 2
         var repeat_twice_slow = document.getElementsByClassName("js-anim-repeat--twice--slow");
         TweenMax.to(repeat_twice_slow, 5, {repeat: 2});
         //infinite loop : -1
         var repeat_infinite_slow = document.getElementsByClassName("js-anim-repeat--inifinite--slow");
         TweenMax.to(repeat_infinite_slow, 5, {repeat: -1});

    /*##################################*/
    /* = EASING
    ####################################*/

    // ease

       /* 1. In */

             //Back
             var easeIn_back = document.getElementsByClassName("js-anim-easeIn--back");
             TweenMax.to(easeIn_back, 1, {ease: Back.easeIn});
             //Bounce
             var easeIn_bounce = document.getElementsByClassName("js-anim-easeIn--bounce");
             TweenMax.to(easeIn_bounce, 1, {ease: Bounce.easeIn});
             //Circ
             var easeIn_circ = document.getElementsByClassName("js-anim-easeIn--circ");
             TweenMax.to(easeIn_circ, 1, {ease: Circ.easeIn});
             //Elastic
             var easeIn_elastic = document.getElementsByClassName("js-anim-easeIn--elastic");
             TweenMax.to(easeIn_elastic, 1, {ease: Elastic.easeIn});
             //Expo
             var easeIn_expo = document.getElementsByClassName("js-anim-easeIn--expo");
             TweenMax.to(easeIn_expo, 1, {ease: Expo.easeIn});
             //Sine
             var easeIn_sine = document.getElementsByClassName("js-anim-easeIn--sine");
             TweenMax.to(easeIn_sine, 1, {ease: Sine.easeIn});

       /* 2. Out */

             //Back
             var easeOut_back = document.getElementsByClassName("js-anim-easeOut--back");
             TweenMax.to(easeOut_back, 1, {ease: Back.easeOut});
             //Bounce
             var easeOut_bounce = document.getElementsByClassName("js-anim-easeOut--bounce");
             TweenMax.to(easeOut_bounce, 1, {ease: Bounce.easeOut});
             //Circ
             var easeOut_circ = document.getElementsByClassName("js-anim-easeOut--circ");
             TweenMax.to(easeOut_circ, 1, {ease: Circ.easeOut});
             //Elastic
             var easeOut_elastic = document.getElementsByClassName("js-anim-easeOut--elastic");
             TweenMax.to(easeOut_elastic, 1, {ease: Elastic.easeOut});
             //Expo
             var easeOut_expo = document.getElementsByClassName("js-anim-easeOut--expo");
             TweenMax.to(easeOut_expo, 1, {ease: Expo.easeOut});
             //Sine
             var easeOut_sine = document.getElementsByClassName("js-anim-easeOut--sine");
             TweenMax.to(easeOut_sine, 1, {ease: Sine.easeOut});

       /* 3. InOut */

             //Back
             var easeInOut_back = document.getElementsByClassName("js-anim-easeInOut--back");
             TweenMax.to(easeInOut_back, 1, {ease: Back.easeInOut});
             //Bounce
             var easeInOut_bounce = document.getElementsByClassName("js-anim-easeInOut--bounce");
             TweenMax.to(easeInOut_bounce, 1, {ease: Bounce.easeInOut});
             //Circ
             var easeInOut_circ = document.getElementsByClassName("js-anim-easeInOut--circ");
             TweenMax.to(easeInOut_circ, 1, {ease: Circ.easeInOut});
             //Elastic
             var easeInOut_elastic = document.getElementsByClassName("js-anim-easeInOut--elastic");
             TweenMax.to(easeInOut_elastic, 1, {ease: Elastic.easeInOut});
             //Expo
             var easeInOut_expo = document.getElementsByClassName("js-anim-easeInOut--expo");
             TweenMax.to(easeInOut_expo, 1, {ease: Expo.easeInOut});
             //Sine
             var easeInOut_sine = document.getElementsByClassName("js-anim-easeInOut--sine");
             TweenMax.to(easeInOut_sine, 1, {ease: Sine.easeInOut});


/*##################################*/
/* = POPUP 30MIN !
####################################*/

  var alertTps = document.getElementsByClassName("alert-tps");
  var alertTps__container = document.getElementsByClassName("alert-tps__container"); /* alert tps */
  var popupTps__cache = document.getElementsByClassName("alert-tps__cache");


/***********************************************************************************************************************
* FIRST : Tzero - PAST
 **********************************************************************************************************************/

// Getting tzero from localstorage
var start_time;
var t0 = sessionStorage.getItem('prevent_long_exposure_popup.start_time');
//Always try to have an item key that directly tells its context and its purpose
// : prevent_long_exposure_popup :
// let's the reader understand the context : A popup that prevent children against long exposure
// Key can ( must/should ) be combined
// : prevent_long_exposure_popup ( context ) + value name
// prevent_long_exposure_popup.start_time
// Now the reader understand the value, and the value is related to a context. Which the reader can also easily identify

//If no value was previously saved in the storage ( like the first time the script is ran )
if (t0 == null) {
    //Init t0 as now
    t0 = new Date().getTime();
    //Save it too
    sessionStorage.setItem('prevent_long_exposure_popup.start_time', t0);
}else {
  console.log("t0 vient de sessionStorage");

}
console.log( "LongExposurePopup : StartTime ", t0);



/***********************************************************************************************************************
 * SECOND : DeltaTime - Difference between then and now
 **********************************************************************************************************************/


 var tpsMaxUtilisation = 300000; //correspond à 5 minutes, ce qu'on laisse pour la démo maus doit être changé en 1800000 afin de faire 30 min


 t1 = new Date().getTime();
 var deltaTime = t1-t0

var delaiDeclenchement = tpsMaxUtilisation - deltaTime;
function apparitionPopup()
{
  $('.alert-tps').removeClass('alert--hide');
  $('.alert-tps').addClass('alert--show');
  console.log('apparition popup');

  window.disableScroll = function(e){
      if (e.keyCode) {
        /^(32|33|34|35|36|38|40)$/.test(e.keyCode) && e.preventDefault();
      }
      else {
        e.preventDefault();
      }
    }

    addEventListener("mousewheel", disableScroll, false);
    addEventListener("DOMMouseScroll", disableScroll, false);
    addEventListener("keydown", disableScroll, false);


  $('.closeAlert').click(function() {

    removeEventListener("mousewheel", disableScroll);
    removeEventListener("DOMMouseScroll", disableScroll);
    removeEventListener("keydown", disableScroll);

    if (!$('.alert-tps').is('alert--show')) {

      // j'enclenche l'anim de disparition
      $('.alert-tps').removeClass('alert--show');
      $('.alert-tps').addClass('alert--hide');


      // tl.fromTo(alertTps__cache, 2, {autoAlpha:0.5},{autoAlpha:0});
      // tl.to(alertTps__container, 2, {y:-200}, '-= 0.7');
      // tl.to(alertTps, 0.2, {y:-1000});
      //
      // Cookies.set('alert-cookies', 'yes'); // j'enregistre la valeur de change-state--on-click dans un cookie valable sur le'entièreté du site'
    }
  });
}
setTimeout(apparitionPopup, delaiDeclenchement);
sessionStorage.removeItem('prevent_long_exposure_popup.start_time');

// /*##################################*/
// /* = POPUP UTILISATION DE COOKIES !
// ####################################*/
//
// // si le cookie est déjà apparu
// if (Cookies.get('alert-cookies') == 'yes') {
//   $('.alert-cookies').removeClass('alert--show');
//   $('.alert-cookies').addClass('alert--hide');
// }
// // si non
// else { // si le cookie n'est pas encore apparu
//   $('.alert-cookies').removeClass('alert--hide');
//   $('.alert-cookies').addClass('alert--show');
//
//   // j'enclenche l'anim d'apparition
//   tl.from(alertCookies__container, 2, {y:-200}, '-= 0.7');
//   tl.fromTo(alertCookies__cache, 2, {autoAlpha:0},{autoAlpha:0.5}, '-= 0.7');
//
//   // je désactive le scroll de la page
//   window.disableScroll = function(e){
//       if (e.keyCode) {
//         /^(32|33|34|35|36|38|40)$/.test(e.keyCode) && e.preventDefault();
//       }
//       else {
//         e.preventDefault();
//       }
//     }
//     addEventListener("mousewheel", disableScroll, false);
//     addEventListener("DOMMouseScroll", disableScroll, false);
//     addEventListener("keydown", disableScroll, false);
// }
//
// $('.closeAlert').click(function() {
//
//   if (!$('.alert-cookies').is('alert--show')) {
//
//     // j'enclenche l'anim de disparition
//     tl.fromTo(alertCookies__cache, 2, {autoAlpha:0.5},{autoAlpha:0});
//     tl.to(alertCookies__container, 2, {y:-200}, '-= 0.7');
//     tl.to(alertCookies, 0.2, {y:-1000});
//
//     Cookies.set('alert-cookies', 'yes'); // j'enregistre la valeur de change-state--on-click dans un cookie valable sur le'entièreté du site'
//   }
//
//   return false;
// });


/*##################################*/
/* = SCROLL DOWN
####################################*/

    var scrollDown = document.getElementById("scrollDown");
    var tl_scrollDown = new TimelineMax({repeat:-1, repeatDelay:1});
    tl_scrollDown.to(scrollDown, 1, {y:'20', ease: Back.easeOut})
                 .to(scrollDown, 1, {y:'-10', ease: Power1.easeOut});

    // initialisation du controlleur
    var controller = new ScrollMagic.Controller();

    /* SCROLL DOWN */

    var t_scrollDown = TweenMax.to('#scrollDown', 1, {opacity:0});
    var s_scrollDown = new ScrollMagic.Scene({
      offset:0 ,
      duration: 150
    })
    .setPin('#scrollDown')
    //.addIndicators()
    .setTween(t_scrollDown)
    .addTo(controller);


/*##################################*/
/* = LOADER LANGUAGES
####################################*/

  $('.languages__list .languages__item button').click(function() { // je vais chercher le bouton pour lui attribuer une classe onClick

    var languageid = $(this).data("language-id"); // je stocke l'attribut data relatif à la langue choisie dans une variable

    $("[data-t-id]").each(function languages(index, element) { // j'applique une fonction à tous les éléments possédant un data-t-id (la valeur de ce data correspond au référencement de l'élément dans la bibliothèque de traductions (master.js))
      var tid = $(element).data("t-id"); // je stocke les éléments possédant ce data-t-id dans une variable
      var traduit = translations[languageid][tid]; // je stocke les traductions relatives à chaque élément (en allant les chercher dans la bibliothèque en fonction de la valeur de leur data-t-id) dans une variable
      element.innerHTML = traduit; // je remplace le contenu de l'élement par cette variable contenant la traduction
    });

    Cookies.set('selected_locale', languageid); // je crée un cookie qui s'apelle selected_locale et qui a pour valeur la variable dans laquelle est stockée la langue choisie

    console.log(languageid);
    return false;
  });

  var currentLocale = 'fr'; //fr par defaut
  if(Cookies.get('selected_locale') !== undefined) // si la valeur du cookie (langue sélectionnée) est définie (différent de undefined)
  {
     // on récupère la langue enregistrée qui vaut
     currentLocale = Cookies.get('selected_locale'); // fr ou nl
     $("[data-t-id]").each(function languages(index, element){ // j'applique une fonction à tous les éléments possédant un data-t-id (la valeur de ce data correspond au référencement de l'élément dans la bibliothèque de traductions (master.js))
       var tid = $(element).data("t-id"); // je stocke les éléments possédant ce data-t-id dans une variable
       var traduit = translations[currentLocale][tid]; // je stocke les traductions relatives à chaque élément (en allant les chercher dans la bibliothèque en fonction de la valeur de leur data-t-id) dans une variable
       element.innerHTML = traduit; // je remplace le contenu de l'élement par cette variable contenant la traduction
     });
  }

};


/*##################################*/
/* = TRADUCTIONS
####################################*/

var translations = {
  "fr": {

  /* Homepage */
    /* popup */
    "popup__title": "Bienvenue sur",
    "popup__body": "Apprends en t'amusant et deviens un as du recyclage ! <br> Tu rejoindras alors la brigade d'Ekool !",
    "popup__close": "Bonne découverte !",
    /* index */
      "intro--planet1":"Pika adore se déguiser. <br>Sauras-tu le retrouver ?",
      "intro--planet2":"Tika est une vraie arsouille. <br>Découvre son monde et tu repartiras avec plein d'astuces !",
      "intro--planet3":"Laka est parfois incompris et débordé. Aide-le à s'y retrouver !",
      "titre--planet4":"Verraka",
      "intro--planet4":"Raka adore la musique. Découvre son univers musical !",
      "intro--planet5":"Nika à la main verte. <br>Toi aussi peut-être ?",
      "intro--planet6":"Lektra est une vrai pile électrique ! Sauras-tu la suivre ?",
      /* about */
      "lien-about__button":"À propos",
      /* footer */
      "footer__copyright":"© 2017 Ekool - Tous droits réservés",

  /* Kapapika */
    /* presentation */
      /* part 1 */
      "K-presentation__title--part1":"De quelle couleur est la poubelle des papiers et cartons ?",
      /* part 2 */
      "K-presentation__title--part2":"Que met-on dans cette poubelle ?",
      "K-presentation__intro--part2":"Il est important de bien suivre les consignes données lors du tri des déchets !",
      "K-presentation__body--part2":"En effet, si l'on ne respecte pas ces consignes, cela peut créer des problèmes et perturber fortement le travail réalisé dans les usines de tri.",
      /* part 3 */
      "K-presentation__intro--part3":"CE QUE JE PEUX METTRE :",
      "K-presentation__li1--part3":"Sacs en papier et boîtes en carton",
      "K-presentation__li2--part3":"Journaux, revues, dépliants",
      "K-presentation__li3--part3":"Papiers, papiers à lettre et pour imprimante",
      /* part 4 */
      "K-presentation__intro--part4":"CE QUE JE NE PEUX PAS METTRE :",
      "K-presentation__li1--part4":"Pas de cartons avec une couche d'alluminium",
      "K-presentation__li2--part4":"Pas de papiers ni de cartons souillés ou gras",
      "K-presentation__li3--part4":"Pas de papiers cellophane",
      /* part 5 */
      "K-presentation__title--part5":"Pourquoi recycle-t-on le papier et le carton ?",
      "K-presentation__intro--part5":"Le recyclage permet de réutiliser la matière. De cette manière, on diminue la montagne de déchets et on préserve un meilleur équilibre dans la nature.",
      "K-presentation__body--part5":"En réutilisant au maximum une matière au lieu de tout simplement la jeter et devoir la recréer de toute pièce, on gaspille moins, on dépense moins d'énergie et on pollue moins.",
      /* part 6 */
      "K-presentation__intro--part6":"Savais-tu que... ?",
      "K-presentation__li1--part6":"Si tout nos journaux étaient recyclés, ...",
      "K-presentation__li2--part6":"... nous pourrions économiser environ 250.000.000 d’arbres dans le monde chaque <br>année !",
    /* transformation */
    "K-transformation__title":"Comment transforme-t-on la matière ?",
    "K-transformation__intro":"Il existe différentes techniques, mais découvrons ensemble les différentes étapes de l'une d'entre elles !",
      /* step 1 */
      "K-step1__intro":"Le papier et le carton sont d’abord triés. Ils seront utilisés pour différentes choses en fonction de leur qualité.",
      "K-step1__li1":"Mauvaise qualité",
      "K-step1__li2":"Bonne qualité",
      /* step 2 */
      "K-step2__intro":"Le papier est ensuite mélangé avec de l’eau, pour obtenir une bouillie brunâtre appelée «pulpe».",
      "K-step2__li1":"La peinture, les agrafes, le vernis, les restes de colle, le plastique et les cordes sont éliminés de la pulpe.",
      "K-step2__li2":"Pour refabriquer certains papiers, il faut aussi retirer l'encre qui reste dans la pulpe et la blanchir.",
      /* step 3 */
      "K-step3__intro":"Pour finir, la pulpe est étalée puis pressée et séchée sur un tapis.",
      "K-step3__body":"Cette dernière étape va former une sorte de feuille de papier géante que l'on va enrouler pour former plein de nouveaux rouleaux de papier !",
    /* resultat */
      /* part 1 */
      "K-resultat__title--part1":"Quel est le résultat de cette transformation ?",
      "K-resultat__intro--part1":"Le papier et le carton usagés deviennent simplement du papier et du carton neufs.",
      "K-resultat__body--part1":"Nous sommes tous les jours en contact avec du papier et du carton recyclés.",
      "K-resultat__li1--part1":"Journaux et magazines",
      "K-resultat__li2--part1":"Cahiers et papier à dessin",
      "K-resultat__li3--part1":"Boîtes en carton et cartons d'emballage",
      "K-resultat__li4--part1":"Essuie tout, papier toilette et mouchoirs",
      /* part 2 */
      "K-resultat__intro--part2":"Mais la vie d’une boîte en carton n’est pas infinie. En effet, la qualité du matériau diminue après chaque traitement. En moyenne, les fibres peuvent être réutilisées cinq à sept fois.",
    /* tuto recup */
    "K-tuto-recup__title":"Tuto récup'!",
    "K-tuto-recup__intro":"Tu peux toi aussi recycler le papier et le carton que tu n'utilises plus à la maison en le transformant en de nouvelles choses !",
    "K-tuto-recup__body":"Ici, tu peux découvrir comment réutiliser des chutes de carton et des rouleaux de papier toilette pour créer une boîte de rangement facilement !",
    /* serious game */
    "K-serious-games__title":"Entraîne-toi et deviens un as du recyclage !",
    "K-serious-games__li1":"Aide Pika à trier <br>les déchets",
    "K-serious-games__li1--button":"START",
    "K-serious-games__li2":"Sauras-tu répondre correctement ?",
    "K-serious-games__li2--button":"START",
    "K-serious-games__li3":"Trouves l'intru <br>rapidement !",
    "K-serious-games__li3--button":"START",
    "K-serious-games__message-erreur":"Désolé, ce jeu n'est pas encore disponible !",
    "K-serious-games__message-erreur--button":"OK",
    /* telechragements */
    "K-telechargements__title":"Activités",
    "K-telechargements__intro":"Découvre des activités à réaliser chez toi en famille ou à l'école avec ta classe, et apprends aux autres ce que tu as appris ici.",
    "K-telechargements__li1":"Décore tes poubelles",
    "K-telechargements__li2":"Le grand jeu du tri",
    "K-telechargements__li3":"Pika a besoin de toi !",
    /* evaluation */
    "K-evaluation__title":"Que penses-tu de ce site ?",
    "K-evaluation__intro":"A toi de nous apprendre quelque chose maintenant <br>et dis-nous comment tu trouves Ekool !",

    "K-evaluation__validation":"Je valide !",
    /* footer */
    "naviguation__a-propos--button":"À propos",
    "naviguation__rejoins-nous":"Rejoins-nous sur",

  /* About */
    /* introduction */
    "introduction__title":"À propos",
    /* brain */
    "brain__title":"Les cerveaux",
    "brain__body":"Nous nous appelons Florence et Alice. Nous avons respectivement 25 et 21 ans et nous sommes toutes deux étudiantes en Infographie et Multimédia en troisième et dernière année à l'Institut des Arts de Diffusion.",
    /* project */
    "project__title":"Le projet",
    "project__body--1":"Ce projet a été réalisé dans le cadre de notre travail de fin d'études. Nous voulions réaliser une plateforme éducative et ludique destinée à un jeune public. Le choix du thème a été orienté par un désir de traiter un sujet qui de prime abord n'intéresse pas ou n'est pas suffisamment expliqué aux enfants de 8 à 10 ans.<br><br>Après avoir posé la question à quelques jeunes de cette tranche d'âge pour savoir quel sujet ils aimeraient nous voir traiter pour un tel projet, nous avons constaté que la question du recyclage était encore floue et incomprise pour un grand nombre d'entre eux mais qu'il étaient désireux d'en savoir plus !",
    "project__body--2":"Nous nous sommes donc finalement orientées vers ce thème. Il nous a ensuite paru important d'inculquer, dès le plus jeune âge, les bons réflexes à adopter pour préserver notre planète ; et surtout d'en expliquer les raisons de la manière la plus attractive possible.<br><br>Afin de mieux comprendre notre public cible et d'en déterminer correctement les besoins et les attentes, nous avons travaillé en collaboration avec Mme Stéphanie Letto, institutrice à l'Athénée Royal Riva-Bella de Braine-l'Alleud. Nous la remercions d'ail- leurs chaleureusement pour son aide !",
    /* contact */
    "contact__title":"Contact",
    "contact__body":"Envie de collaborer ? Des questions ? <br>N'hésitez pas à nous contacter : <strong>ekool@gmail.com</strong><br><br><br>Ou via les réseaux sociaux :"

  },

  "nl": {

  /* Homepage */
    /* popup */
    "popup__title": "Welkom op",
    "popup__body": "Leer terwijl je speelt en word een krak in recycling! <br>Doe dan mee met de Ekool brigade!",
    "popup__close": "Prettige ontdekking!",
    /* index */
      "intro--planet1":"Pika verkleedt zich graag. <br>Zal je hem kunnen terugvinden?",
      "intro--planet2":"Tika is een echte schurk. <br>Ontdek haar wereld en krijg een stelletje trucs!",
      "intro--planet3":"Laka is soms misbegrepen en heeft het druk. Help hem zijn weg te vinden!",
      "titre--planet4": "Glaska",
      "intro--planet4":"Aska is dol op muziek. <br>Ontdek haar muzikale wereld!",
      "intro--planet5":"Nika heeft groene vingers. <br>En jij?",
      "intro--planet6":"Lektra bruist van energie! <br>Zal je haar kunnen volgen?",
      /* about */
      "lien-about__button":"Over ons",
      /* footer */
      "footer__copyright":"© 2017 Ekool – Alle rechten voorbehouden",

  /* Kapapika */
    /* presentation */
      /* part 1 */
      "K-presentation__title--part1":"Wat voor kleur heeft de vuilnisbak voor papier en karton?",
      /* part 2 */
      "K-presentation__title--part2":"Wat gooi je in deze vuilnisbak?",
      "K-presentation__intro--part2":"Het is belangrijk dat je de instructies die je krijgt voor het sorteren van de afval , respecteert!",
      "K-presentation__body--part2":"Inderdaad, het niet respecteren van deze instructies kunnen voor problemen en zware verstoringen zorgen in de fabrieken  waar de afval gesorteerd wordt.",
      /* part 3 */
      "K-presentation__intro--part3":"WAT IK MAG GOOIEN:",
      "K-presentation__li1--part3":"Papieren zakjes en kartonnen dozen",
      "K-presentation__li2--part3":"Kranten, tijdschriften, folders",
      "K-presentation__li3--part3":"Papieren, briefpapier en inkjetpapier",
      /* part 4 */
      "K-presentation__intro--part4":"WAT IK NIET MAG GOOIEN:",
      "K-presentation__li1--part4":"Geen kartonnen met aluminiumlaag",
      "K-presentation__li2--part4":"Geen vervuilde of vettige papieren en kartonnen",
      "K-presentation__li3--part4":"Geen cellofaan",
      /* part 5 */
      "K-presentation__title--part5":"Waarom worden papier en karton gesorteerd?",
      "K-presentation__intro--part5":"Dankzij recycling kan het materiaal opnieuw gebruikt worden. Op die manier neemt het afvalvolume af en houdt men het natuurlijke evenwicht in stand.",
      "K-presentation__body--part5":"Door een materiaal maximaal te hergebruiken in plaats van het gewoon weg te gooien en het opnieuw te moeten maken, verspilt en vervuilt men minder en bespaart men energie.",
      /* part 6 */
      "K-presentation__intro--part6":"Wist je dat...?",
      "K-presentation__li1--part6":"Als alle kranten gerecycleerd zouden worden, ...",
      "K-presentation__li2--part6":"... zouden we elk jaar wereldwijd 250.000.000 bomen kunnen besparen!",
    /* transformation */
    "K-transformation__title":"Hoe wordt de materie verwerkt?",
    "K-transformation__intro":"Er zijn verschillende technieken. Maar laat ons samen de verschillende stappen waaruit één van deze techniek bestaat, samen ontdekken!",
      /* step 1 */
      "K-step1__intro":"Papier en karton worden eerst gesorteerd. Op basis van hun kwaliteit zullen ze gebruikt worden om verschillende dingen te maken.",
      "K-step1__li1":"Slechte kwaliteit",
      "K-step1__li2":"Goede kwaliteit",
      /* step 2 */
      "K-step2__intro":"Het papier wordt dan met water gemengd om een bruinachtige pap te bekomen die « pulp » heet.",
      "K-step2__li1":"Verf, nietjes, lak, lijmresten, plastiek en touwen worden uit de pulp verwijderd.",
      "K-step2__li2":"Om bepaalde soorten papier opnieuw te produceren, moet de inkt die in de pulp verwijderd worden.",
      /* step 3 */
      "K-step3__intro":"En als laatste wordt de pulp op een band aangebracht, geperst en opgedroogd.",
      "K-step3__body":"Met deze laatste stap wordt een reus vel papier aangemaakt dat opgerold zal worden  om veel nieuwe rollen papier te vormen!",
    /* resultat */
      /* part 1 */
      "K-resultat__title--part1":"Wat is het resultaat van deze verwerking?",
      "K-resultat__intro--part1":"Oud papier en oud karton worden  gewoon niew papier en nieuw karton.",
      "K-resultat__body--part1":"Elke dag komen we in contact met gerecycleerd papier en karton.",
      "K-resultat__li1--part1":"Dagbladen en tijdschriften",
      "K-resultat__li2--part1":"Boeken en tekenpapier",
      "K-resultat__li3--part1":"Kartonnen dozen",
      "K-resultat__li4--part1":"Keukenpapier, toiletpapier en zakdoekjes",
      /* part 2 */
      "K-resultat__intro--part2":"Maar het leven van een kartonnen doos is niet oneindig. De kwaliteit van het materiaal vermindert na elke verwerking. De vezels kunnen gemiddeld 5 à 7 keer hergebruikt worden.",
    /* tuto recup */
    "K-tuto-recup__title":"Tuto recup!",
    "K-tuto-recup__intro":"Jij kan ook papier en karton die je thuis niet meer gebruikt, recycleren en er nieuwe dingen van maken!",
    "K-tuto-recup__body":"Ontdek hier hoe je restjes karton en rollen wc-papier kan hergebruiken om gemakkelijk een opbergdoos te maken!",
    /* serious game */
    "K-serious-games__title":"Oefen en word een krak in recyling!",
    "K-serious-games__li1":"Help Pika bij het sorteren <br>van de afval",
    "K-serious-games__li1--button":"START",
    "K-serious-games__li2":"Zal je het juiste antwoord kunnen geven?",
    "K-serious-games__li2--button":"START",
    "K-serious-games__li3":"Vind de <br>vreemde!",
    "K-serious-games__li3--button":"START",
    "K-serious-games__message-erreur":"Sorry, dit spel is nog niet beschikbaar!",
    "K-serious-games__message-erreur--button":"OK",
    /* telechragements */
    "K-telechargements__title":"Activiteiten",
    "K-telechargements__intro":"Ontdek activiteiten die je thuis met je familie of op school met je klasgenoten kan verrichten. Leer aanderen aan wat je zelf hier geleerd hebt.",
    "K-telechargements__li1":"Versier je vuilnisbakken",
    "K-telechargements__li2":"Het grote sorteerspel",
    "K-telechargements__li3":"Pika heeft je nodig!",
    /* evaluation */
    "K-evaluation__title":"Wat vind je van deze site?",
    "K-evaluation__intro":"Nu is het jouw beurt om ons iets aan te leren door <br>ons te vertellen wat je van Ekool vindt!",

    "K-evaluation__validation":"Ik bevestig!",
    /* footer */
    "naviguation__a-propos--button":"Over ons",
    "naviguation__rejoins-nous":"Tref ons op",

  /* About */
    /* introduction */
    "introduction__title":"Over ons",
    /* brain */
    "brain__title":"De koppen",
    "brain__body":"Wij heten Florence en Alice. We zijn respectievelijk  25 en 21 jaar oud. We zijn beiden infografie en multimedia studenten in het derde en laatste studiejaar aan l'Institut des Arts de Diffusion.",
    /* project */
    "project__title":"Het project",
    "project__body--1":"Dit project werd uitgevoerd in het kader van ons eindwerk. We wilden een educatief en speels platform ontwikkelen, gericht op de jeugd. Wij hebben voor deze  thema gekozen omdat we het over een onderwerp wilden hebben dat op eerste zicht weinig interesse opwekt bij of niet genoeg uitgelegd wordt aan 8 tot 10 jarige kinderen. <br><br>Nadat we een aantal jongeren ondervraagd hebben om te weten te komen welk onderwerp in zo'n project behandeld zou moeten worden, is er gebleken dat een meerderheid van hen maar een vaag idee hadden of gewoon niet begrepen waarover recycling gaat. Ze wilden er nochtans heel graag meer over weten!",
    "project__body--2":"We hebben ons dus uiteindelijk op deze thema gericht.  Bovendien lijkt het ons belangrijk dat kinderen op zeer prille leeftijd en op een antrekkelijke manier goede gewoontes aanleren om ons planeet te  beschermen en vooral dat ze de redenen hiervoor begrijpen.<br><br>Om ons doelpubliek beter te begrijpen en zijn behoeftes en verwachtigen correct te bepalen, hebben we samengewerkt met Mevrouw Stéphanie Letto, lerares aan de Athénée Royal Riva-Bella van Braine-l'Alleud. Wij danken haar trouwens hartelijk voor haar hulp!",
    /* contact */
    "contact__title":"Contact",
    "contact__body":"Zin om samen te werken? Vragen? <br>Neem met ons contact op: <strong>ekool@gmail.com</strong><br><br><br>Of via de sociale media:"

  }
};
