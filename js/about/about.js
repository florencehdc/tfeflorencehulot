window.addEventListener("load", function(){

/* AUDIO */

    var player3 = document.querySelector('#audioPlayer_about');
    player3.play();
    player3.loop = true;

    function mute3(){
      if (player3.volume == 0){
        player3.volume = 1;
        document.getElementById("notMute3").id = "mute3";
      }else {
        player3.volume=0;
        document.getElementById("mute3").id = "notMute3";
      }
    }
    function boutonMute3()
    {
      var muteSound3 = document.getElementById("mute3");
      muteSound3.addEventListener("click", mute3);
    }
    boutonMute3();


/* ANIMATIONS FIXED */

    /* planète */
    var js_rotate = document.getElementsByClassName("js-rotate");
    TweenMax.to(js_rotate, 200, {rotation:360, repeat:-1, ease: Power0.easeNone});


/* ANIMATIONS ON SCROLL */

    // initialisation du controlleur
    var controller = new ScrollMagic.Controller();

    /* introduction */

        // vaisceau
        var tl_vaisceau_vide = new TimelineMax(),
            vaisceau_vide__step1 = TweenMax.fromTo('#vaisceau-vide', 0.1, {opacity:0},{opacity:1}),
            vaisceau_vide__step2 = TweenMax.fromTo('#vaisceau-vide', 1, {scale:0.1},{scale:3, rotation:-25, y:480, x:-110});
        tl_vaisceau_vide.add(vaisceau_vide__step1);
        tl_vaisceau_vide.add(vaisceau_vide__step2);
        var s_vaisceau_vide = new ScrollMagic.Scene({
          triggerElement: '#trigger--introduction',
          duration: 1000,
          triggerHook: 0
        })
        .setTween(tl_vaisceau_vide)
        .addTo(controller);

        // meteorites
           // meteorite1
              var meteorite1 = TweenMax.fromTo('#meteorite--1', 1,{scale:0},{scale:1, rotation:-30, y:600, x:-1600});
              var s_meteorite1 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 900,
                triggerHook: 0
              })
              .setTween(meteorite1)
              .addTo(controller);
           // meteorite2
              var meteorite2 = TweenMax.fromTo('#meteorite--2', 1,{scale:0},{scale:1, rotation:-30, y:800, x:-1600});
              var s_meteorite2 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1200,
                triggerHook: 0
              })
              .setTween(meteorite2)
              .addTo(controller);
           // meteorite3
              var meteorite3 = TweenMax.fromTo('#meteorite--3', 1,{scale:0},{scale:1, rotation:-30, y:900, x:-1600});
              var s_meteorite3 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1000,
                triggerHook: 0
              })
              .setTween(meteorite3)
              .addTo(controller);
           // meteorite4
              var meteorite4 = TweenMax.fromTo('#meteorite--4', 1,{scale:0},{scale:1, rotation:-30, y:700, x:-1600});
              var s_meteorite4 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1100,
                triggerHook: 0
              })
              .setTween(meteorite4)
              .addTo(controller);
           // meteorite5
              var meteorite5 = TweenMax.fromTo('#meteorite--5', 1,{scale:0},{scale:1, rotation:-30, y:650, x:-1600});
              var s_meteorite5 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 950,
                triggerHook: 0
              })
              .setTween(meteorite5)
              .addTo(controller);
           // meteorite6
              var meteorite6 = TweenMax.fromTo('#meteorite--6', 1,{scale:0},{scale:1, rotation:-30, y:850, x:-1600});
              var s_meteorite6 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1150,
                triggerHook: 0
              })
              .setTween(meteorite6)
              .addTo(controller);
           // meteorite7
              var meteorite7 = TweenMax.fromTo('#meteorite--7', 1,{scale:0},{scale:1, rotation:-30, y:750, x:-1600});
              var s_meteorite7 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 900,
                triggerHook: 0
              })
              .setTween(meteorite7)
              .addTo(controller);
           // meteorite8
              var meteorite8 = TweenMax.fromTo('#meteorite--8', 1,{scale:0},{scale:1, rotation:-30, y:800, x:-1600});
              var s_meteorite8 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1100,
                triggerHook: 0
              })
              .setTween(meteorite8)
              .addTo(controller);
           // meteorite9
              var meteorite9 = TweenMax.fromTo('#meteorite--9', 1,{scale:0},{scale:1, rotation:-30, y:700, x:-1600});
              var s_meteorite9 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1150,
                triggerHook: 0
              })
              .setTween(meteorite9)
              .addTo(controller);
           // meteorite10
              var meteorite10 = TweenMax.fromTo('#meteorite--10', 1,{scale:0},{scale:1, rotation:-30, y:650, x:-1600});
              var s_meteorite10 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 950,
                triggerHook: 0
              })
              .setTween(meteorite10)
              .addTo(controller);
           // meteorite11
              var meteorite11 = TweenMax.fromTo('#meteorite--11', 1,{scale:0},{scale:1, rotation:-30, y:800, x:-1600});
              var s_meteorite11 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1000,
                triggerHook: 0
              })
              .setTween(meteorite11)
              .addTo(controller);
           // meteorite12
              var meteorite12 = TweenMax.fromTo('#meteorite--12', 1,{scale:0},{scale:1, rotation:-30, y:750, x:-1600});
              var s_meteorite12 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1500,
                triggerHook: 0
              })
              .setTween(meteorite12)
              .addTo(controller);
           // meteorite13
              var meteorite13 = TweenMax.fromTo('#meteorite--13', 1,{scale:0},{scale:1, rotation:-30, y:650, x:-1600});
              var s_meteorite13 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1550,
                triggerHook: 0
              })
              .setTween(meteorite13)
              .addTo(controller);
           // meteorite14
              var meteorite14 = TweenMax.fromTo('#meteorite--14', 1,{scale:0},{scale:1, rotation:-30, y:650, x:-1600});
              var s_meteorite14 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 900,
                triggerHook: 0
              })
              .setTween(meteorite14)
              .addTo(controller);
           // meteorite15
              var meteorite15 = TweenMax.fromTo('#meteorite--15', 1,{scale:0},{scale:1, rotation:-30, y:650, x:-1600});
              var s_meteorite15 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1400,
                triggerHook: 0
              })
              .setTween(meteorite15)
              .addTo(controller);
           // meteorite16
              var meteorite16 = TweenMax.fromTo('#meteorite--16', 1,{scale:0},{scale:1, rotation:-30, y:650, x:-1600});
              var s_meteorite16 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1300,
                triggerHook: 0
              })
              .setTween(meteorite16)
              .addTo(controller);
           // meteorite17
              var meteorite17 = TweenMax.fromTo('#meteorite--17', 1,{scale:0},{scale:1, rotation:-30, y:650, x:-1600});
              var s_meteorite17 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1150,
                triggerHook: 0
              })
              .setTween(meteorite17)
              .addTo(controller);
           // meteorite18
              var meteorite18 = TweenMax.fromTo('#meteorite--18', 1,{scale:0},{scale:1, rotation:-30, y:650, x:-1600});
              var s_meteorite18 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 950,
                triggerHook: 0
              })
              .setTween(meteorite18)
              .addTo(controller);
           // meteorite19
              var meteorite19 = TweenMax.fromTo('#meteorite--19', 1,{scale:0},{scale:1, rotation:-30, y:650, x:-1600});
              var s_meteorite19 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1300,
                triggerHook: 0
              })
              .setTween(meteorite19)
              .addTo(controller);
           // meteorite20
              var meteorite20 = TweenMax.fromTo('#meteorite--20', 1,{scale:0},{scale:1, rotation:-30, y:650, x:-1600});
              var s_meteorite20 = new ScrollMagic.Scene({
                triggerElement: '#trigger2--introduction',
                duration: 1500,
                triggerHook: 0
              })
              .setTween(meteorite20)
              .addTo(controller);

        // nuage
           var nuage1 = TweenMax.to('#nuage--1', 1,{y:-100});
           var s_snuage1 = new ScrollMagic.Scene({
             triggerElement: '#trigger--introduction',
             duration: 400,
             triggerHook: 0
           })
           .setTween(nuage1)
           .addTo(controller);

        // stars
           // star1
              var star1 = TweenMax.to('#star--1', 1,{y:50, x:-100});
              var s_star1 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 400,
                triggerHook: 0
              })
              .setTween(star1)
              .addTo(controller);
           // star2
              var star2 = TweenMax.to('#star--2', 1,{y:50, x:-50});
              var s_star2 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 400,
                triggerHook: 0
              })
              .setTween(star2)
              .addTo(controller);
           // star3
              var star3 = TweenMax.to('#star--3', 1,{y:-20, x:50});
              var s_star3 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 400,
                triggerHook: 0
              })
              .setTween(star3)
              .addTo(controller);
           // star4
              var star4 = TweenMax.to('#star--4', 1,{y:-10, x:20});
              var s_star4 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 400,
                triggerHook: 0
              })
              .setTween(star4)
              .addTo(controller);
           // star5
              var star5 = TweenMax.to('#star--5', 1,{y:10, x:50});
              var s_star5 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 400,
                triggerHook: 0
              })
              .setTween(star5)
              .addTo(controller);
           // star6
              var star6 = TweenMax.to('#star--6', 1,{y:-30, x:50});
              var s_star6 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 400,
                triggerHook: 0
              })
              .setTween(star6)
              .addTo(controller);
           // star7
              var star7 = TweenMax.to('#star--7', 1,{y:-10, x:20});
              var s_star7 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 400,
                triggerHook: 0
              })
              .setTween(star7)
              .addTo(controller);
           // star8
              var star8 = TweenMax.to('#star--8', 1,{y:-10, x:20});
              var s_star8 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 400,
                triggerHook: 0
              })
              .setTween(star8)
              .addTo(controller);
           // star9
              var star9 = TweenMax.to('#star--9', 1,{y:-10, x:100});
              var s_star9 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 400,
                triggerHook: 0
              })
              .setTween(star9)
              .addTo(controller);

        // planets
           // planet1
              var planet1 = TweenMax.to('#planet--1', 1,{x:-50});
              var s_planet1 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 600,
                triggerHook: 0
              })
              .setTween(planet1)
              .addTo(controller);
           // planet2
              var planet2 = TweenMax.to('#planet--2', 1,{x:-50, y:10});
              var s_planet2 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 600,
                triggerHook: 0
              })
              .setTween(planet2)
              .addTo(controller);
           // planet3
              var planet3 = TweenMax.to('#planet--3', 1,{x:-80, y:-10});
              var s_planet3 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 600,
                triggerHook: 0
              })
              .setTween(planet3)
              .addTo(controller);
           // planet4
              var planet4 = TweenMax.to('#planet--4', 1,{x:150, y:-5});
              var s_planet4 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 600,
                triggerHook: 0
              })
              .setTween(planet4)
              .addTo(controller);
           // planet5
              var planet5 = TweenMax.to('#planet--5', 1,{x:100, y:-20});
              var s_planet5 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 600,
                triggerHook: 0
              })
              .setTween(planet5)
              .addTo(controller);
           // planet6
              var planet6 = TweenMax.to('#planet--6', 1,{x:200, y:-5});
              var s_planet6 = new ScrollMagic.Scene({
                triggerElement: '#trigger--introduction',
                duration: 600,
                triggerHook: 0
              })
              .setTween(planet6)
              .addTo(controller);

    /* brain */

       // nuage
          // nuage2
             var nuage2 = TweenMax.to('#nuage--2', 1,{y:-100});
             var s_snuage2 = new ScrollMagic.Scene({
               triggerElement: '#trigger--brain',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(nuage2)
             .addTo(controller);
          // nuage3
             var nuage3 = TweenMax.to('#nuage--3', 1,{y:50, x:-100});
             var s_snuage3 = new ScrollMagic.Scene({
               triggerElement: '#trigger--brain',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(nuage3)
             .addTo(controller);

       // stars
          // star10
             var star10 = TweenMax.to('#star--10', 1,{y:50, x:-100});
             var s_star10 = new ScrollMagic.Scene({
               triggerElement: '#trigger--brain',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(star10)
             .addTo(controller);
          // star11
             var star11 = TweenMax.to('#star--11', 1,{y:50, x:-50});
             var s_star11 = new ScrollMagic.Scene({
               triggerElement: '#trigger--brain',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(star11)
             .addTo(controller);
          // star12
             var star12 = TweenMax.to('#star--12', 1,{y:-20, x:50});
             var s_star12 = new ScrollMagic.Scene({
               triggerElement: '#trigger--brain',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(star12)
             .addTo(controller);
          // star13
             var star13 = TweenMax.to('#star--13', 1,{y:-10, x:20});
             var s_star13 = new ScrollMagic.Scene({
               triggerElement: '#trigger--brain',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(star13)
             .addTo(controller);
          // star14
             var star14 = TweenMax.to('#star--14', 1,{y:10, x:50});
             var s_star14 = new ScrollMagic.Scene({
               triggerElement: '#trigger--brain',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(star14)
             .addTo(controller);
          // star15
             var star15 = TweenMax.to('#star--15', 1,{y:-30, x:50});
             var s_star15 = new ScrollMagic.Scene({
               triggerElement: '#trigger--brain',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(star15)
             .addTo(controller);
          // star16
             var star16 = TweenMax.to('#star--16', 1,{y:-10, x:20});
             var s_star16 = new ScrollMagic.Scene({
               triggerElement: '#trigger--brain',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(star16)
             .addTo(controller);
          // star17
             var star17 = TweenMax.to('#star--17', 1,{y:-10, x:20});
             var s_star17 = new ScrollMagic.Scene({
               triggerElement: '#trigger--brain',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(star17)
             .addTo(controller);
          // star18
             var star18 = TweenMax.to('#star--18', 1,{y:-10, x:20});
             var s_star18 = new ScrollMagic.Scene({
               triggerElement: '#trigger--project',
               duration: 600,
               triggerHook: 0.3
             })
             .setTween(star18)
             .addTo(controller);

       // cerveaux
          // cerveau1
             // morceaux
                // 1
                var cerveau1_a = TweenMax.from('#cerveau1-a', 1,{y:75,x:27, scale:0});
                var s_cerveau1_a = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.5
                })
                .setTween(cerveau1_a)
                .addTo(controller);
                // 2
                var cerveau2_a = TweenMax.from('#cerveau2-a', 1,{y:71, x:-27, scale:0});
                var s_cerveau2_a = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.5
                })
                .setTween(cerveau2_a)
                .addTo(controller);
                // 3
                var cerveau3_a = TweenMax.from('#cerveau3-a', 1,{y:-44, x:-10, scale:0});
                var s_cerveau3_a = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.5
                })
                .setTween(cerveau3_a)
                .addTo(controller);
                // 4
                var cerveau4_a = TweenMax.from('#cerveau4-a', 1,{y:-44, x:10, scale:0});
                var s_cerveau4_a = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.5
                })
                .setTween(cerveau4_a)
                .addTo(controller);
                // 5
                var cerveau5_a = TweenMax.from('#cerveau5-a', 1,{y:50, x:-50, scale:0});
                var s_cerveau5_a = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.5
                })
                .setTween(cerveau5_a)
                .addTo(controller);
                // 6
                var cerveau6_a = TweenMax.from('#cerveau6-a', 1,{y:50, x:50, scale:0});
                var s_cerveau6_a = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.5
                })
                .setTween(cerveau6_a)
                .addTo(controller);
                // 7
                var cerveau7_a = TweenMax.from('#cerveau7-a', 1,{y:-40, x:-38, scale:0});
                var s_cerveau7_a = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.5
                })
                .setTween(cerveau7_a)
                .addTo(controller);
                // 8
                var cerveau8_a = TweenMax.from('#cerveau8-a', 1,{y:-40, x:38, scale:0});
                var s_cerveau8_a = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.5
                })
                .setTween(cerveau8_a)
                .addTo(controller);
             // yeux
                var yeux_a = TweenMax.from('#yeux-a', 1,{scale:0});
                var s_yeux_a = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 90,
                  triggerHook: 0.1
                })
                .setTween(yeux_a)
                .addTo(controller);
             // casque
             // mouvement


          // cerveau2
             // morceaux
                // 1
                var cerveau1_b = TweenMax.from('#cerveau1-b', 1,{y:75,x:27, scale:0});
                var s_cerveau1_b = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.3
                })
                .setTween(cerveau1_b)
                .addTo(controller);
                // 2
                var cerveau2_b = TweenMax.from('#cerveau2-b', 1,{y:71, x:-27, scale:0});
                var s_cerveau2_b = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.3
                })
                .setTween(cerveau2_b)
                .addTo(controller);
                // 3
                var cerveau3_b = TweenMax.from('#cerveau3-b', 1,{y:-44, x:-10, scale:0});
                var s_cerveau3_b = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.3
                })
                .setTween(cerveau3_b)
                .addTo(controller);
                // 4
                var cerveau4_b = TweenMax.from('#cerveau4-b', 1,{y:-44, x:10, scale:0});
                var s_cerveau4_b = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.3
                })
                .setTween(cerveau4_b)
                .addTo(controller);
                // 5
                var cerveau5_b = TweenMax.from('#cerveau5-b', 1,{y:50, x:-50, scale:0});
                var s_cerveau5_b = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.3
                })
                .setTween(cerveau5_b)
                .addTo(controller);
                // 6
                var cerveau6_b = TweenMax.from('#cerveau6-b', 1,{y:50, x:50, scale:0});
                var s_cerveau6_b = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.3
                })
                .setTween(cerveau6_b)
                .addTo(controller);
                // 7
                var cerveau7_b = TweenMax.from('#cerveau7-b', 1,{y:-40, x:-38, scale:0});
                var s_cerveau7_b = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.3
                })
                .setTween(cerveau7_b)
                .addTo(controller);
                // 8
                var cerveau8_b = TweenMax.from('#cerveau8-b', 1,{y:-40, x:38, scale:0});
                var s_cerveau8_b = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 250,
                  triggerHook: 0.3
                })
                .setTween(cerveau8_b)
                .addTo(controller);
             // yeux
                var yeux_b = TweenMax.from('#yeux-b', 1,{scale:0});
                var s_yeux_b = new ScrollMagic.Scene({
                  triggerElement: '#trigger--brain',
                  duration: 70,
                  triggerHook: 0
                })
                .setTween(yeux_b)
                .addTo(controller);

       // vaisceau
          var vaisceau_rempli = TweenMax.from('#vaisceau-rempli', 1,{scale:0, y:-355, x:970});
          var s_vaisceau_rempli = new ScrollMagic.Scene({
            triggerElement: '#trigger--vaisceau',
            duration: 800,
            triggerHook: 0
          })
          .setTween(vaisceau_rempli)
          .addTo(controller);


    /* project */

       // nuage
          // nuage5
             var nuage5 = TweenMax.to('#nuage--5', 1,{y:-200});
             var s_snuage5 = new ScrollMagic.Scene({
               triggerElement: '#trigger--project',
               duration: 500,
               triggerHook: 0.4
             })
             .setTween(nuage5)
             .addTo(controller);

       // vaisceau2
       /*
          var vaisceau_vide2 = TweenMax.from('#vaisceau-vide2', 1,{scale:0, y:-895, x:-870});
          var s_vaisceau_vide2 = new ScrollMagic.Scene({
            triggerElement: '#trigger--vaisceau2',
            duration: 200,
            triggerHook: 0.2
          })
          .setTween(vaisceau_vide2)
          .addTo(controller);
       */
          var tl_vaisceau_vide2 = new TimelineMax(),
              vaisceau_vide2__step1 = TweenMax.from('#vaisceau-vide2', 0.1, {scale:0, y:-995, x:-1970, rotation: 40}),
              vaisceau_vide2__step2 = TweenMax.to('#vaisceau-vide2', 0.1, {opacity:0, scale: 0, y:100},'-=0.9');
          tl_vaisceau_vide2.add(vaisceau_vide2__step1);
          tl_vaisceau_vide2.add(vaisceau_vide2__step2);
          var s_vaisceau_vide2 = new ScrollMagic.Scene({
            triggerElement: '#trigger--vaisceau2',
            duration: 490,
            triggerHook: 0.5
          })
          .setTween(tl_vaisceau_vide2)
          .addTo(controller);

    /* contact */

       // nuage
          // nuage4
             var nuage4 = TweenMax.to('#nuage--4', 1,{y:-100, x:100});
             var s_snuage4 = new ScrollMagic.Scene({
               triggerElement: '#trigger--contact',
               duration: 400,
               triggerHook: 0.5
             })
             .setTween(nuage4)
             .addTo(controller);

});
