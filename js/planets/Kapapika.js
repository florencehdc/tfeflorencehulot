
window.addEventListener("load", function(){

/* AUDIO */

  var player2 = document.querySelector('#audioPlayer_kapapika');
  player2.play();
  player2.loop = true;

  function mute2(){
    if (player2.volume == 0){
      player2.volume = 1;
      document.getElementById("notMute2").id = "mute2";
    }else {
      player2.volume=0;
      document.getElementById("mute2").id = "notMute2";
    }
  }
  function boutonMute2()
  {
    var muteSound2 = document.getElementById("mute2");
    muteSound2.addEventListener("click", mute2);
  }
  boutonMute2();


  /* ANIMATIONS FIXED */

  // animations immersif

  //animation arbres 1
  var tlarbreim = new TimelineMax();
  tlarbreim.staggerFrom('.arbre-im',0.9,{scale:0, transformOrigin:'50px 150px', ease:Elastic.easeOut.config(1,0.6)},0.9)

  //animation arbre 2
  var tlarbreim2 = new TimelineMax();
  tlarbreim2.staggerFrom('.arbre-im2',0.9,{scale:0, transformOrigin:'50px 150px', ease:Elastic.easeOut.config(1,0.9)},1)

  //animation arbre 3
  var tlarbreim3 = new TimelineMax();
  tlarbreim3.staggerFrom('.arbre-im3',1,{scale:0, transformOrigin:'50px 150px', ease:Elastic.easeOut.config(1,0.6)},0.6)

  //animation nenuphares
  var animeNenuphare = TweenMax.fromTo('#nenuphare',100, {y:0, x:0},{rotation:100, y:100, x:200});

  var animeNenuphare2 = TweenMax.fromTo('#nenuphare2',100, {y:0, x:0},{rotation:100, y:-100, x:-200});

  //animation fusée
  var animation__fusee = TweenMax.fromTo('#fusee', 100, {x:-100, y:200, scale:0.1},{scale:0.5, rotation:10, y:-150, x:500});
  // var animation_fusée =
  // TweenMax.to("#fusee", 1, {rotation:360, transformOrigin:"50% 50%"},y:400, x:-100});


  /* ANIMATIONS ON SCROLL */

    // initialisation du controlleur
    var controller = new ScrollMagic.Controller();

    /* BACKINDEX */
      var s_backindex =  new ScrollMagic.Scene ({
        triggerElement: '#trigger--backindex',
        duration : 0,
        triggerHook: 0.02
      })
      .setPin('#backindex')
      .addTo(controller);

    /* presentation part1 */
      // construction du tween
      var K_presentation__part1__poubelle = TweenMax.fromTo('#K-presentation--part1__poubelle', 1, {opacity:0},{opacity:1});
      // construction de la scène
      var s_K_presentation__part1__poubelle = new ScrollMagic.Scene({
        triggerElement: '#trigger--K-presentation--part1',
        duration: 400,
        triggerHook: 0.4
      })
      .setTween(K_presentation__part1__poubelle)
      .addTo(controller);

      //animation pas bloc2
      var tl_pas_bl2 = new TimelineMax();
      tl_pas_bl2.staggerTo('.pas_bloc2',0.2,{opacity:1},0.5);

      var s_pas_bl2 = new ScrollMagic.Scene({
        triggerElement:'#trigger--K-presentation--part1',
        triggerHook: 0.1
      })
      .setTween(tl_pas_bl2)
      .addTo(controller);

      //animation champi1
      var champi1_bloc2 = TweenMax.from('.champi1_bloc2', 1.5, {scale:0, transformOrigin:'100px 100px', ease: Bounce.easeOut});
      var s_champi1_bloc2 = new ScrollMagic.Scene({
        triggerElement: '#trigger--K-presentation--part1',
        triggerHook: 0.1
      })
      .setTween(champi1_bloc2)
      .addTo(controller);

      //animation arbre 1
      var arbre1_bloc2 = TweenMax.from('.arbre1_bloc2', 1.5, {scale:0, transformOrigin:'100px 300px', ease: Elastic.easeOut.config(1, 0.9)});
      var s_arbre1_bloc2 = new ScrollMagic.Scene({
        triggerElement: '#trigger--K-presentation--part1',
        triggerHook: 0.5
      })
      .addIndicators()
      .setTween(arbre1_bloc2)
      .addTo(controller);

      //animation arbre 2
      var arbre2_bloc2 = TweenMax.from('.arbre2_bloc2', 1.5, {scale:0, transformOrigin:'100px 300px', ease: Elastic.easeOut.config(1, 0.9)});
      var s_arbre2_bloc2 = new ScrollMagic.Scene({
        triggerElement: '#trigger--K-presentation--part1',
        triggerHook: 0.3
      })
      .setTween(arbre2_bloc2)
      .addTo(controller);


      //animation arbre 3
      var arbre3_bloc2 = TweenMax.from('.arbre3_bloc2', 1.5, {scale:0, transformOrigin:'100px 300px', ease: Elastic.easeOut.config(1, 0.3)});
      var s_arbre3_bloc2 = new ScrollMagic.Scene({
        triggerElement: '#trigger--K-presentation--part1',
        triggerHook: 0.2
      })
      .setTween(arbre3_bloc2)
      .addTo(controller);


    /* presentation part2 */
      var K_presentation__part2__emoji = TweenMax.fromTo('#K-presentation--part2__emoji', 1, {x:'100px', opacity:0, rotation:180},{x:'0px', opacity:1, rotation:360});
      var s_K_presentation__part2__emoji = new ScrollMagic.Scene({
        triggerElement: '#trigger--K-presentation--part2',
        duration: 180,
        triggerHook: 0.4
      })
      .setTween(K_presentation__part2__emoji)
      .addTo(controller);

      //animation pas bloc3
      var tl_pas_bl3 = new TimelineMax();
      tl_pas_bl3.staggerTo('.pas_bloc3',0.2,{opacity:1},0.5);

      var s_pas_bl3 = new ScrollMagic.Scene({
        triggerElement:'#trigger--K-presentation--part2',
        triggerHook: 0.1
      })
      .setTween(tl_pas_bl3)
      .addTo(controller);

    /* presentation part3 */
       // construction de la timeline
       var tl_K_presentation__part3__liste = new TimelineMax(),
       // construction des tweens
           K_presentation__part3__li1 = TweenMax.from("#K-presentation--part3__li1", 1, {y: -100, opacity:0}),
           K_presentation__part3__li2 = TweenMax.from("#K-presentation--part3__li2", 1, {y: -100, opacity:0}),
           K_presentation__part3__li3 = TweenMax.from("#K-presentation--part3__li3", 1, {y: -100, opacity:0});
       // ajout des tweens à la timeline
       tl_K_presentation__part3__liste.add(K_presentation__part3__li1)
                                      .add(K_presentation__part3__li2)
                                      .add(K_presentation__part3__li3);
       // construction de la scène
       var s_K_presentation__part3__liste = new ScrollMagic.Scene({
         triggerElement: '#trigger--K-presentation--part3',
         triggerHook: 0.5
       })
       .setTween(tl_K_presentation__part3__liste)
       .addTo(controller);

       //animation arbres bloc4
       var tl_arbres_bl4 = new TimelineMax();
       tl_arbres_bl4.staggerFrom('.arbres_bloc4',1.5, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(1, 0.6)},2);

       var s_arbres_bl4 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-presentation--part3',
         triggerHook: 0.5
       })
       .setTween(tl_arbres_bl4)
       .addTo(controller);

       //animation arbres2 bloc4
       var tl_arbres2_bl4 = new TimelineMax();
       tl_arbres2_bl4.staggerFrom('.arbres2_bloc4',1.5, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(1, 0.4)},1.3);

       var s_arbres2_bl4 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-presentation--part3',
         triggerHook: 0.5
       })
       .setTween(tl_arbres2_bl4)
       .addTo(controller);

    /* presentation part4 */
       var tl_K_presentation__part4__liste = new TimelineMax(),
           K_presentation__part4__li1 = TweenMax.from("#K-presentation--part4__li1", 1, {y: -100, opacity:0}),
           K_presentation__part4__li2 = TweenMax.from("#K-presentation--part4__li2", 1, {y: -100, opacity:0}),
           K_presentation__part4__li3 = TweenMax.from("#K-presentation--part4__li3", 1, {y: -100, opacity:0});
       // ajout des tweens à la timeline
       tl_K_presentation__part4__liste.add(K_presentation__part4__li1)
                                      .add(K_presentation__part4__li2)
                                      .add(K_presentation__part4__li3);
       // construction de la scène
       var s_K_presentation__part4__liste = new ScrollMagic.Scene({
         triggerElement: '#trigger--K-presentation--part4',
         triggerHook: 0.3
       })
       .setTween(tl_K_presentation__part4__liste)
       .addTo(controller);

       //animation arbres bloc5
       var tl_arbres_bl5 = new TimelineMax();
       tl_arbres_bl5.staggerFrom('.arbres_bloc5',1.5, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(1, 0.4)},1);
       var s_arbres_bl5 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-presentation--part4',
         triggerHook: 0.5
       })
       .setTween(tl_arbres_bl5)
       .addTo(controller);

       //animation arbres2 bloc5
       var tl_arbres2_bl5 = new TimelineMax();
       tl_arbres2_bl5.staggerFrom('.arbres2_bloc5',1.5, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(1, 0.4)},1.5);
       var s_arbres2_bl5 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-presentation--part4',
         triggerHook: 0.4
       })
       .setTween(tl_arbres2_bl5)
       .addTo(controller);

       //animation panneau bl5
       var panneau_bl5 = TweenMax.from('.panneau_bl5', 1.5, {scale:0, transformOrigin:'100px 300px', ease: Elastic.easeOut.config(1, 0.3)});
       var s_panneau_bl5 = new ScrollMagic.Scene({
         triggerElement: '#trigger--K-presentation--part4',
         triggerHook: 0.2
       })
       .setTween(panneau_bl5)
       .addTo(controller);

    /* presentation part5 */
      var K_presentation__part5__emoji = TweenMax.from('#K-presentation--part5__emoji', 1, {scale:0, opacity:0});
      var s_K_presentation__part5__emoji = new ScrollMagic.Scene({
        triggerElement: '#trigger--K-presentation--part5',
        duration: 250,
        triggerHook: 0.4
      })
      .setTween(K_presentation__part5__emoji)
      .addTo(controller);

      //animation pas bloc6
      var tl_pas_bl6 = new TimelineMax();
      tl_pas_bl6.staggerTo('.pas_bloc6',0.2,{opacity:1},0.5);
      var s_pas_bl6 = new ScrollMagic.Scene({
        triggerElement:'#trigger--K-presentation--part5',
        triggerHook: 0.5
      })
      .setTween(tl_pas_bl6)
      .addTo(controller);

      //animation pas interstitiel1
      var tl_pas_int5 = new TimelineMax();
      tl_pas_int5.staggerTo('.pas_int5',0.2,{opacity:1},0.5);
      var s_pas_int5 = new ScrollMagic.Scene({
        triggerElement:'#trigger--K-presentation--part4',
        triggerHook: 0.1
      })
      .setTween(tl_pas_int5)
      .addTo(controller);

    /* presentation part6 */
       var tl_K_presentation__part6__liste = new TimelineMax(),
           K_presentation__part6__li1 = TweenMax.from("#K-presentation--part6__li1", 1, {x: 100, opacity:0}),
           K_presentation__part6__li2 = TweenMax.from("#K-presentation--part6__li2", 1, {x: 100, opacity:0});
       // ajout des tweens à la timeline
       tl_K_presentation__part6__liste.add(K_presentation__part6__li1)
                                      .add(K_presentation__part6__li2);
       // construction de la scène
       var s_K_presentation__part6__liste = new ScrollMagic.Scene({
         triggerElement: '#trigger--K-presentation--part6',
         triggerHook: 0.3
       })
       .setTween(tl_K_presentation__part6__liste)
       .addTo(controller);

       //animation pas bloc7
       var tl_pas_bl7 = new TimelineMax();
       tl_pas_bl7.staggerTo('.pas_bloc7',0.2,{opacity:1},0.5);
       var s_pas_bl7 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-presentation--part6',
         triggerHook: 0.5
       })
       .setTween(tl_pas_bl7)
       .addTo(controller);


    /* trasnformation */
       var K_transformation = TweenMax.staggerTo($(".step"), 1, {rotationY:-180, repeat:1, yoyo:true}, 0.1);
       var s_K_transformation = new ScrollMagic.Scene({
         triggerElement: '#trigger--K-transformation',
         triggerHook: 0
       })
       .setTween(K_transformation)
       .addTo(controller);

       //animation forêt interstitiel
       var tl_foret1_int1 = new TimelineMax();
       tl_foret1_int1.staggerFrom('.foret1_int1',1.5, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(1, 0.4)},1);
       var s_foret1_int1 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-resultat--part1',
         triggerHook: 0.9
       })
       .setTween(tl_foret1_int1)
       .addTo(controller);

       //animation forêt interstitiel2
       var tl_foret2_int1 = new TimelineMax();
       tl_foret2_int1.staggerFrom('.foret2_int1',1.5, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(1, 0.4)},0.7);
       var s_foret2_int1 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-resultat--part1',
         triggerHook: 0.9
       })
       .setTween(tl_foret2_int1)
       .addTo(controller);

       //animation forêt interstitiel3
       var tl_foret3_int1 = new TimelineMax();
       tl_foret3_int1.staggerFrom('.foret3_int1',1.5, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(1, 0.4)},0.8);
       var s_foret3_int1 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-resultat--part1',
         triggerHook: 0.9
       })
       .setTween(tl_foret3_int1)
       .addTo(controller);

    /* resultat part1 */
       var tl_K_resultat__part1__liste = new TimelineMax(),
           K_resultat__part1__li1 = TweenMax.from("#K-resultat--part1__li1", 1, {scale: 0, opacity:0}),
           K_resultat__part1__li2 = TweenMax.from("#K-resultat--part1__li2", 1, {scale: 0, opacity:0}),
           K_resultat__part1__li3 = TweenMax.from("#K-resultat--part1__li3", 1, {scale: 0, opacity:0}),
           K_resultat__part1__li4 = TweenMax.from("#K-resultat--part1__li4", 1, {scale: 0, opacity:0});
       tl_K_resultat__part1__liste.add(K_resultat__part1__li1)
                                  .add(K_resultat__part1__li2)
                                  .add(K_resultat__part1__li3)
                                  .add(K_resultat__part1__li4);
       var s_K_resultat__part1__liste = new ScrollMagic.Scene({
         triggerElement: '#trigger--K-resultat--part1',
         triggerHook: 0.2
       })
       .setTween(tl_K_resultat__part1__liste)
       .addTo(controller);

       //animation pas bloc9
       var tl_pas_bl9 = new TimelineMax();
       tl_pas_bl9.staggerTo('.pas_bloc9',0.2,{opacity:1},0.5);
       var s_pas_bl9 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-resultat--part1',
         triggerHook: 0.5
       })
       .setTween(tl_pas_bl9)
       .addTo(controller);

    /* resultat part2 */
       var K_resultat__part2__li1 = TweenMax.fromTo('#K-resultat--part2__li1', 1, {opacity:0},{opacity:1});
       var s_K_resultat__part2__li1 = new ScrollMagic.Scene({
         triggerElement: '#trigger--K-resultat--part2',
         duration: 300,
         triggerHook: 0.4
       })
       .setTween(K_resultat__part2__li1)
       .addTo(controller);

      //  //animation pas bloc10
      //  var tl_pas_bl10 = new TimelineMax();
      //  tl_pas_bl10.staggerTo('.pas_bloc10',0.2,{opacity:1},0.5);
      //  var s_pas_bl10 = new ScrollMagic.Scene({
      //    triggerElement:'#trigger--K-resultat--part2',
      //    triggerHook: 0.5
      //  })
      //  .setTween(tl_pas_bl10)
      //  .addTo(controller);

       //animation forêt interstitiel2 ap cycle
       var tl_foret1_int2 = new TimelineMax();
       tl_foret1_int2.staggerFrom('.foret1_int2',1.5, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(0.4, 0.4)},1);
       var s_foret1_int2 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-tuto-recup',
         triggerHook: 0.9
       })
       .setTween(tl_foret1_int2)
       .addTo(controller);

       //animation forêt2 interstitiel2 ap cycle
       var tl_foret2_int2 = new TimelineMax();
       tl_foret2_int2.staggerFrom('.foret2_int2',1.5, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(0.4, 0.4)},0.7);
       var s_foret2_int2 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-tuto-recup',
         triggerHook: 0.9
       })
       .setTween(tl_foret2_int2)
       .addTo(controller);

       //animation forêt3 interstitiel2 ap cycle
       var tl_foret3_int2 = new TimelineMax();
       tl_foret3_int2.staggerFrom('.foret3_int2',1.5, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(0.4, 0.4)},0.8);
       var s_foret3_int2 = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-tuto-recup',
         triggerHook: 0.9
       })
       .setTween(tl_foret3_int2)
       .addTo(controller);

    /* tuto recup */
       var K_tuto_recup = TweenMax.fromTo('#K-tuto-recup', 1, {y:50, opacity:0},{y:0, opacity:1});
       var s_K_tuto_recup = new ScrollMagic.Scene({
         triggerElement: '#trigger--K-tuto-recup',
         duration: 250,
         triggerHook: 0.8
       })
       .setTween(K_tuto_recup)
       .addTo(controller);

       //animation pas interstitiel apres tuto

       var tl_pas_int_vid = new TimelineMax();
       tl_pas_int_vid.staggerTo('.pas_int_vid',0.2,{opacity:1},0.5);
       var s_pas_int_vid = new ScrollMagic.Scene({
         triggerElement:'#trigger--K-tuto-recup',
         triggerHook: 0.1
       })
       .setTween(tl_pas_int_vid)
       .addTo(controller);

    /* serious games */
       var tl_K_serious_games__liste = new TimelineMax(),
           K_serious_games__li1 = TweenMax.from("#K-serious-games__li1", 1, {x: 180, opacity:0, ease:Back.easeOut}),
           K_serious_games__li2 = TweenMax.from("#K-serious-games__li2", 1, {x: 180, opacity:0, ease:Back.easeOut}),
           K_serious_games__li3 = TweenMax.from("#K-serious-games__li3", 1, {x: 180, opacity:0, ease:Back.easeOut});
       tl_K_serious_games__liste.add(K_serious_games__li1)
                                .add(K_serious_games__li2)
                                .add(K_serious_games__li3);
       var s_K_serious_games__liste = new ScrollMagic.Scene({
         triggerElement: '#trigger--K-serious-games',
         triggerHook: 0.3
       })
       .setTween(tl_K_serious_games__liste)
       .addTo(controller);

    /* telechargements */
       var tl_K_telechargements__liste = new TimelineMax(),
           K_telechargements__li3 = TweenMax.from("#K-telechargements__li3", 1, {x: -200, opacity:0, ease:Back.easeOut}),
           K_telechargements__li2 = TweenMax.from("#K-telechargements__li2", 1, {x: -200, opacity:0, ease:Back.easeOut}),
           K_telechargements__li1 = TweenMax.from("#K-telechargements__li1", 1, {x: -200, opacity:0, ease:Back.easeOut});
       tl_K_telechargements__liste.add(K_telechargements__li3)
                                  .add(K_telechargements__li2)
                                  .add(K_telechargements__li1);
       var s_K_telechargements__liste = new ScrollMagic.Scene({
         triggerElement: '#trigger--K-telechargements',
         triggerHook: 0.3
       })
       .setTween(tl_K_telechargements__liste)
       .addTo(controller);

    /* evaluation */
    var tl_K_evaluation__liste = new TimelineMax();
        tl_K_evaluation__liste.fromTo("#K-evaluation__li1", 1, {y:0, opacity:0},{y: -50, opacity:1})
                              .to("#K-evaluation__li1", 1, {y: 0, ease:Bounce.easeOut})
                              .fromTo("#K-evaluation__li2", 1, {y:0, opacity:0},{y: -50, opacity:1}, "=-1")
                              .to("#K-evaluation__li2", 1, {y: 0, ease:Bounce.easeOut})
                              .fromTo("#K-evaluation__li3", 1, {y:0, opacity:0},{y: -50, opacity:1}, "=-1")
                              .to("#K-evaluation__li3", 1, {y: 0, ease:Bounce.easeOut})
                              .fromTo("#K-evaluation__li4", 1, {y:0, opacity:0},{y: -50, opacity:1}, "=-1")
                              .to("#K-evaluation__li4", 1, {y: 0, ease:Bounce.easeOut})
                              .fromTo("#K-evaluation__li5", 1, {y:0, opacity:0},{y: -50, opacity:1}, "=-1")
                              .to("#K-evaluation__li5", 1, {y: 0, ease:Bounce.easeOut});

    var s_K_evaluation__liste = new ScrollMagic.Scene({
      triggerElement: '#trigger--K-evaluation',
      triggerHook: 0.4
    })
    .setTween(tl_K_evaluation__liste)
    .addTo(controller);

    //animation arbres2 footer
    var tl_arbres_footer = new TimelineMax();
    tl_arbres_footer.staggerFrom('.arbres_footer',1, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(1, 0.4)},0.7);

    var s_arbres_footer = new ScrollMagic.Scene({
      triggerElement:'#trigger--footer',
      triggerHook: 0.9
    })
    .setTween(tl_arbres_footer)
    .addTo(controller);

    var tl_arbres_footer2 = new TimelineMax();
    tl_arbres_footer2.staggerFrom('.arbres_footer2',0.9, {scale:0, transformOrigin:'50px 150px', ease: Elastic.easeOut.config(1, 0.4)},0.8);

    var s_arbres_footer2 = new ScrollMagic.Scene({
      triggerElement:'#trigger--footer',
      triggerHook: 0.9
    })
    .setTween(tl_arbres_footer2)
    .addTo(controller);


// MODULE TRANSFORMATION

    TweenMax.set(".stepWrapper", {perspective:800});
    TweenMax.set(".step", {transformStyle:"preserve-3d"});
    TweenMax.set(".back", {rotationY:-180});
    TweenMax.set([".back", ".front"], {backfaceVisibility:"hidden"});

    $(".stepWrapper").hover(
      function() {
        TweenMax.to($(this).find(".step"), 1.5, {rotationY:180, ease:Back.easeOut});
      },
      function() {
        TweenMax.to($(this).find(".step"), 1.5, {rotationY:0, ease:Back.easeOut});
      }
    );


// MODULE SERIOUS GAMES

    TweenMax.to(".K-serious-games__games button", 2, {scale:1.1, delay:2, repeat:-1, repeatDelay:5, ease:Bounce.easeOut});

    // JEU 1

    $('.serious-games1').click(function() {
      console.log("j'ai cliqué");
      $('.K-serious-games__fenetreJeu').removeClass('fenetre--hide');
      $('.K-serious-games__fenetreJeu').addClass('fenetre--show');
      var canvas = document.getElementById("jeu");

    // désactivation su scroll de la page pendant le jeu

    window.disableScroll = function(e){
      	if (e.keyCode) {
      		/^(32|33|34|35|36|38|40)$/.test(e.keyCode) && e.preventDefault();
      	}
        else {
      		e.preventDefault();
      	}
      }
      addEventListener("mousewheel", disableScroll, false);
      addEventListener("DOMMouseScroll", disableScroll, false);
      addEventListener("keydown", disableScroll, false);

    });

    $('.closeJeu').click(function() {
        $('.K-serious-games__fenetreJeu').removeClass('fenetre--show');
        $('.K-serious-games__fenetreJeu').addClass('fenetre--hide');

        // réactivation su scroll de la page pendant le jeu

        removeEventListener("mousewheel", disableScroll);
        removeEventListener("DOMMouseScroll", disableScroll);
        removeEventListener("keydown", disableScroll);

        // remettre le jeu à 0

        gameState = "menu";

    });

    // JEU 2

    $('.serious-games2').click(function() {
        $('.K-serious-games__message-erreur').removeClass('message--hide');
        $('.K-serious-games__message-erreur').addClass('message--show');

        window.disableScroll = function(e){
          	if (e.keyCode) {
          		/^(32|33|34|35|36|38|40)$/.test(e.keyCode) && e.preventDefault();
          	}
            else {
          		e.preventDefault();
          	}
          }
          addEventListener("mousewheel", disableScroll, false);
          addEventListener("DOMMouseScroll", disableScroll, false);
          addEventListener("keydown", disableScroll, false);
    });

    // JEU 3

    $('.serious-games3').click(function() {
        $('.K-serious-games__message-erreur').removeClass('message--hide');
        $('.K-serious-games__message-erreur').addClass('message--show');

        window.disableScroll = function(e){
          	if (e.keyCode) {
          		/^(32|33|34|35|36|38|40)$/.test(e.keyCode) && e.preventDefault();
          	}
            else {
          		e.preventDefault();
          	}
          }
          addEventListener("mousewheel", disableScroll, false);
          addEventListener("DOMMouseScroll", disableScroll, false);
          addEventListener("keydown", disableScroll, false);
    });

    // CLOSE MESSAGE ERREUR

    $('.closeMessage').click(function() {
        $('.K-serious-games__message-erreur').removeClass('message--show');
        $('.K-serious-games__message-erreur').addClass('message--hide');

        removeEventListener("mousewheel", disableScroll);
        removeEventListener("DOMMouseScroll", disableScroll);
        removeEventListener("keydown", disableScroll);
    });

// MODULE EVALUATION

    var $curseurDefault = document.getElementById('K-evaluation__li5');
    $($curseurDefault).removeClass('curseur--hide')
                      .addClass('curseur--show');

    /* BTN GROUPE */
    var $emojisGroup = $('.emojis__list .emojis__item button');

    $emojisGroup.click(function() {

      var emoji = this;
      console.log(emoji);

      $emojisGroup.removeClass('curseur--show')
                  .addClass('curseur--hide');

      $(this).removeClass('curseur--hide');
      $(this).addClass('curseur--show');

      var selected_grade = emoji.getAttribute('data-emoji-value');

      Cookies.set('selected_emoji', selected_grade);

            $.ajax({
              url : 'insert_emojis.php', // La ressource ciblée
              type : 'POST', // Le type de la requête HTTP. "on envoie une information au serveur"
              data :{grade: selected_grade},  // On fait passer nos variables au script insert_emojis.php


             success : function(resultat, statut){
                 console.log('la réponse de php est',resultat);
             },

             error : function(resultat, statut, erreur){

             }
            });

      return false;

    });

    if(Cookies.get('selected_emoji') !== undefined)
    {
      var currentEmoji = document.querySelector('button[data-emoji-value="'+Cookies.get('selected_emoji')+'"]');
      console.log(currentEmoji);
      $(currentEmoji).removeClass('curseur--hide');
      $(currentEmoji).addClass('curseur--show');

      if(Cookies.get('selected_emoji') !== '5/5') {
        $($curseurDefault).removeClass('curseur--show')
                          .addClass('curseur--hide');
      }
    }

    $('.K-evaluation__validation button').click(function() {
      console.log("J'ai validé le smiley : "+Cookies.get('selected_emoji'));
    });

});
