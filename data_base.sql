-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:8889
-- Généré le :  Mer 14 Juin 2017 à 17:02
-- Version du serveur :  5.6.28
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `ekool_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gradation` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `feedback`
--

INSERT INTO `feedback` (`id`, `date`, `gradation`) VALUES
(1, '2017-06-14 14:17:01', 1),
(2, '2017-06-14 14:22:48', 5),
(3, '2017-06-14 14:24:29', 3),
(4, '2017-06-14 14:40:24', 3),
(5, '2017-06-14 14:46:05', 3),
(6, '2017-06-14 14:46:23', 3),
(7, '2017-06-14 14:58:52', 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
